package l6_oopexample;

import java.util.Scanner;

public class Moto extends BaseTransport{
    
    private int m_nHandGrip = 0; //Длина руля
    
    public void setHandGrip(int nHandGrip){
        m_nHandGrip = nHandGrip;
    }
    
    public int getHandGrip(){
        return m_nHandGrip;
    }

    @Override
    public String getModelName() {
        String strName = "moto-" + super.getModelName();
        return strName;
    }
    
    @Override
    public void dataInput() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите производителя мотоцикла: ");
        String strTradeMark = scan.nextLine();
        System.out.println("Введите марку мотоцикла: ");
        String strModelName = scan.nextLine();
        System.out.println("Введите мощность мотоцикла: ");
        float fPower = scan.nextFloat();
        System.out.println("Введите кол-во колес мотоцикла: ");
        int nCountWheels = scan.nextInt();
        System.out.println("Введите цвет мотоцикла: ");
        scan.nextLine();
        String strColor = scan.nextLine();
        System.out.println("Введите ширину руля мотоцикла: ");
        int nHandGrip = scan.nextInt();
        
        setTradeMark(strTradeMark);
        setModelName(strModelName);
        setPower(fPower);
        setCountWheels(nCountWheels);
        setColor(strColor);
        setHandGrip(nHandGrip);
    }

    @Override
    public void dataOutput() {
        System.out.println("Мотоцикл марки " + getTradeMark() + " модель " 
                + getModelName() + " мощность " + getPower() + " имеет "
                + getCountWheels() + " колес и цвет " + getColor() 
                + " ширина руля " + getHandGrip() +" мм");
    }

    @Override
    public boolean isSearch(String strSearch) {
        boolean bResult = false;
        bResult = getColor().equalsIgnoreCase(strSearch) 
                || getTradeMark().toLowerCase().contains(strSearch.toLowerCase())
                || getModelName().toLowerCase().contains(strSearch.toLowerCase());
        return bResult;
    }
    
}
