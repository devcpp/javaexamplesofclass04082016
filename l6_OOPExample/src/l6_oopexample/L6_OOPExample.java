package l6_oopexample;

import java.util.ArrayList;
import java.util.Scanner;

public class L6_OOPExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ArrayList<BaseTransport> arrData = new ArrayList<>();
        
        do{
            System.out.println("Menu:");
            System.out.println("1. Add transport");
            System.out.println("2. Show list");
            System.out.println("3. Search");
            System.out.println("4. Exit");
            int nVal = scan.nextInt();
            switch(nVal){
                case 1:
                    System.out.println("Sub-menu:");
                    System.out.println("1. Moto");
                    System.out.println("2. Car");
                    nVal = scan.nextInt();
                    BaseTransport transport = null;
                    switch (nVal){
                        case 1:
                            transport = new Moto();
                            break;
                        case 2:
                            transport = new Car();
                            break;
                    }
                    if (transport!=null){
                        transport.dataInput();
                        arrData.add(transport);
                    }
                    break;
                case 2:
                    for (BaseTransport transpotrForOutput:arrData){
                        transpotrForOutput.dataOutput();
                    }
                    break;
                case 3:
                    scan.nextLine();
                    String strSearch = scan.nextLine();
                    for (BaseTransport transpotrForSearch:arrData){
                        if(transpotrForSearch.isSearch(strSearch)){
                            transpotrForSearch.dataOutput();
                        }
                    }
                    break;
                case 4:
                    return;
            }
        } while(true);
    }
    
}
