package l6_oopexample;

import java.util.Scanner;

public class Car extends BaseTransport{
    
    private int m_nCountDors = 0; //Длина руля
    
    public void setCountDors(int nCountDors){
        m_nCountDors = nCountDors;
    }
    
    public int getCountDors(){
        return m_nCountDors;
    }

    @Override
    public void dataInput() {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Введите производителя автомобиля: ");
        String strTradeMark = scan.nextLine();
        System.out.println("Введите марку автомобиля: ");
        String strModelName = scan.nextLine();
        System.out.println("Введите мощность автомобиля: ");
        float fPower = scan.nextFloat();
        System.out.println("Введите кол-во колес автомобиля: ");
        int nCountWheels = scan.nextInt();
        System.out.println("Введите цвет автомобиля: ");
        scan.nextLine();
        String strColor = scan.nextLine();
        System.out.println("Введите кол-во дверей автомобиля: ");
        int nCountDors = scan.nextInt();
        
        setTradeMark(strTradeMark);
        setModelName(strModelName);
        setPower(fPower);
        setCountWheels(nCountWheels);
        setColor(strColor);
        setCountDors(nCountDors);
    }

    @Override
    public void dataOutput() {
        System.out.println("Автомобиль марки " + getTradeMark() + " модель " 
                + getModelName() + " мощность двигателя " + getPower() + " имеет "
                + getCountWheels() + " колес " + getCountDors() 
                + " дверей и цвет " + getColor());
    }

    @Override
    public boolean isSearch(String strSearch) {
        boolean bResult = false;
        bResult = getColor().equalsIgnoreCase(strSearch) 
                || getTradeMark().toLowerCase().contains(strSearch.toLowerCase())
                || getModelName().toLowerCase().contains(strSearch.toLowerCase());
        return bResult;
    }
    
}
