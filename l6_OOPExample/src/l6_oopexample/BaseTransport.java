package l6_oopexample;

public abstract class BaseTransport {
    
    private String m_strTradeMark = "";
    private String m_strModelName = "";
    private float m_fPower = 0.0f;
    private int m_nCountWheels = 0;
    private String m_strColor = "";
    
    public void setTradeMark(String strTradeMark){
        m_strTradeMark = strTradeMark;
    }
    
    public void setModelName(String strModelName){
        m_strModelName = strModelName;
    }
    
    public void setPower(float fPower){
        m_fPower = fPower;
    }
    
    public void setCountWheels(int nCountWheels){
        m_nCountWheels = nCountWheels;
    }
    
    public void setColor(String strColor){
        m_strColor = strColor;
    }
    
    public String getTradeMark(){
        return m_strTradeMark;
    }
    
    public String getModelName(){
        return m_strModelName;
    }
    
    public float getPower(){
        return m_fPower;
    }
    
    public int getCountWheels(){
        return m_nCountWheels;
    }
    
    public String getColor(){
        return m_strColor ;
    }
    
    public abstract void dataInput();
    
    public abstract void dataOutput();
    
    public abstract boolean isSearch(String strSearch);
}
