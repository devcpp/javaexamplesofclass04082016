package com.kkrasylnykov.l27_externalappexample;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText m_editText = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View btnWeb = findViewById(R.id.btnWeb);
        View btnCall = findViewById(R.id.btnCall);
        View btnMail = findViewById(R.id.btnMail);

        btnWeb.setOnClickListener(this);
        btnCall.setOnClickListener(this);
        btnMail.setOnClickListener(this);

        m_editText = (EditText) findViewById(R.id.inputEditText);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnWeb:
                Intent webIntent = new Intent();
                webIntent.setAction(Intent.ACTION_VIEW);
                webIntent.setData(Uri.parse(m_editText.getText().toString()));
                startActivity(webIntent);
                break;
            case R.id.btnCall:
                Intent intentCall = new Intent();
                //intentCall.setAction(Intent.ACTION_CALL);
                intentCall.setAction(Intent.ACTION_DIAL);
                intentCall.setData(Uri.parse("tel:" + m_editText.getText().toString()));
                startActivity(intentCall);
                break;
            case R.id.btnMail:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, "emailaddress@emailaddress.com");
                intent.putExtra(Intent.EXTRA_SUBJECT, m_editText.getText().toString());
                intent.putExtra(Intent.EXTRA_TEXT, "I'm email body.");

                startActivity(Intent.createChooser(intent, "Send Email"));
                break;
        }
    }
}
