package com.kkrasylnykov.l29_dialogsexample;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View btnAlert = findViewById(R.id.alertDialog);
        View btnCustom = findViewById(R.id.customDialog);
        View btnDate = findViewById(R.id.dateDialog);
        View btnWait = findViewById(R.id.waitDialog);

        btnAlert.setOnClickListener(this);
        btnCustom.setOnClickListener(this);
        btnDate.setOnClickListener(this);
        btnWait.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.alertDialog:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Title");
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    builder.setMessage("Test >=5");
                } else {
                    builder.setMessage("Test<5");
                }
                builder.setPositiveButton("Positive", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this,"Positive",Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Negative", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this,"Negative",Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNeutralButton("Neutral", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this,"Neutral",Toast.LENGTH_LONG).show();
                    }
                });
                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Toast.makeText(MainActivity.this,"setOnCancelListener",Toast.LENGTH_LONG).show();
                    }
                });
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        Toast.makeText(MainActivity.this,"setOnDismissListener",Toast.LENGTH_LONG).show();
                    }
                });
                builder.setCancelable(false);
                builder.create().show();
                break;
            case R.id.customDialog:
                CustomDialog customDialog = new CustomDialog(this);
                customDialog.show();
                break;
            case R.id.dateDialog:
                Calendar calendar = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, dd.MM.yy");
                        Date date = new Date((i-1900), i1, i2);
                        Toast.makeText(MainActivity.this,simpleDateFormat.format(date),Toast.LENGTH_LONG).show();
                        //m_btnDateDialog.setText(simpleDateFormat.format(date));

                        //String strMon = i1<=8?"0"+(i1+1):Integer.toString(i1+1);
                        //m_btnDateDialog.setText("Date: " + i2 + "." + strMon + "." + i);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
                break;
            case R.id.waitDialog:
                ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();
                break;
        }
    }
}
