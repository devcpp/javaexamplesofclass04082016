package kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.db_wrappers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.db.DBHelper;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.Entity;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public abstract class BaseDBWrapper <T extends Entity>  {

    private DBHelper m_DbHelper = null;
    private String m_strTableName = "";

    public BaseDBWrapper(Context context, String strTableName){
        m_DbHelper = new DBHelper(context);
        m_strTableName = strTableName;
    }

    protected SQLiteDatabase getReadable(){
        return m_DbHelper.getReadableDatabase();
    }

    protected SQLiteDatabase getWritable(){
        return m_DbHelper.getWritableDatabase();
    }

    protected String getTableName(){
        return m_strTableName;
    }

    protected abstract T getInstance(Cursor cursor);

    public ArrayList<T> getAll(){
        ArrayList<T> arrData = new ArrayList<>();
        SQLiteDatabase db = getReadable();
        Cursor cursor = db.query(getTableName(), null, null, null, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        T item = getInstance(cursor);
                        arrData.add(item);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrData;
    }

    public T getItemById(long nId){
        return getItemByField(DBConstants.TABLE_CONTACTS_FIELD_ID, nId);
    }

    protected T getItemByField(String strFieldName, long nId){
        T item = null;
        SQLiteDatabase db = getReadable();

        String strSelection = strFieldName + "=?";
        String[] arrSelectionArgs = {Long.toString(nId)};

        Cursor cursor = db.query(getTableName(), null,
                strSelection, arrSelectionArgs, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    item = getInstance(cursor);
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return item;
    }

    public long insertItem(T item){
        SQLiteDatabase db = getWritable();
        ContentValues values = item.getContentValues();
        long id = db.insert(getTableName(),null,values);
        db.close();
        return id;
    }

    public void updateItem(T item){
        SQLiteDatabase db = getWritable();
        ContentValues values = item.getContentValues();
        String strSelection = DBConstants.TABLE_CONTACTS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};
        db.update(getTableName(),values,strSelection, arrSelectionArgs);
        db.close();
    }

    public void deleteItem(T item){
        SQLiteDatabase db = getWritable();
        String strSelection = DBConstants.TABLE_CONTACTS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};
        db.delete(getTableName(),strSelection,arrSelectionArgs);
        db.close();
    }

    public void deleteAll(){
        SQLiteDatabase db = getWritable();
        db.delete(getTableName(),null,null);
        db.close();
    }

}
