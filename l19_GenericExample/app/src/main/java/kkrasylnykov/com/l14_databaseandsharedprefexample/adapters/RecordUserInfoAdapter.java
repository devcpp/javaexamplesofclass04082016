package kkrasylnykov.com.l14_databaseandsharedprefexample.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.R;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;


public class RecordUserInfoAdapter extends BaseAdapter {

    private ArrayList<RecordUserInfo> m_arrData = null;

    public RecordUserInfoAdapter(ArrayList<RecordUserInfo> arrData){
        m_arrData = arrData;
    }

    @Override
    public int getCount() {
        return m_arrData.size();
    }

    @Override
    public Object getItem(int position) {
        return m_arrData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((RecordUserInfo)getItem(position)).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater li = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.item_record_user_info, parent, false);
        }
        RecordUserInfo info = ((RecordUserInfo)getItem(position));
        TextView tv = (TextView)convertView.findViewById(R.id.textViewItem);
        tv.setText(info.toString());
        return convertView;
    }
}
