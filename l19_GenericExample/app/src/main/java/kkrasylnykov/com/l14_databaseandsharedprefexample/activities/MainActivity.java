package kkrasylnykov.com.l14_databaseandsharedprefexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.R;
import kkrasylnykov.com.l14_databaseandsharedprefexample.adapters.RecordUserInfoAdapter;
import kkrasylnykov.com.l14_databaseandsharedprefexample.adapters.RecordUserInfoExpandableAdapter;
import kkrasylnykov.com.l14_databaseandsharedprefexample.db.DBHelper;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.engines.RecordUserInfoEngine;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.AppSettings;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private ExpandableListView m_ExpandableListView = null;
    private RecordUserInfoExpandableAdapter m_adapter = null;
    private ArrayList<RecordUserInfo> m_arrData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_ExpandableListView = (ExpandableListView) findViewById(R.id.expandableListViewMainActivity);
        m_adapter = new RecordUserInfoExpandableAdapter(m_arrData);
        m_ExpandableListView.setAdapter(m_adapter);
        m_ExpandableListView.setOnItemLongClickListener(this);
        View btnAdd = findViewById(R.id.addButtonMainActivity);
        btnAdd.setOnClickListener(this);

        View btnRemoveAll = findViewById(R.id.removeAllButtonMainActivity);
        btnRemoveAll.setOnClickListener(this);

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.getIsFirstStart()){
            appSettings.setIsFirstStart(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateList();
    }

    private void updateList(){
        RecordUserInfoEngine engine = new RecordUserInfoEngine(this);
        ArrayList<RecordUserInfo> arrData = engine.getAll();

        m_arrData.clear();
        m_arrData.addAll(arrData);
        m_adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addButtonMainActivity:
                Intent intent = new Intent(this, ComposeActivity.class);
                startActivity(intent);
                break;
            case R.id.removeAllButtonMainActivity:
                RecordUserInfoEngine engine = new RecordUserInfoEngine(this);
                engine.deleteAll();
                updateList();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        /*Intent intentEdit = new Intent(this, ComposeActivity.class);
        intentEdit.putExtra(ComposeActivity.EXTRA_KEY_ID, id);
        startActivity(intentEdit);*/
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        long packedPosition = m_ExpandableListView.getExpandableListPosition(position);
        int itemType = ExpandableListView.getPackedPositionType(packedPosition);

        if (itemType==ExpandableListView.PACKED_POSITION_TYPE_GROUP){
            int groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition);
            Intent intentEdit = new Intent(this, ComposeActivity.class);
            intentEdit.putExtra(ComposeActivity.EXTRA_KEY_ID, m_arrData.get(groupPosition).getId());
            startActivity(intentEdit);
        }
        return true;
    }
}
