package kkrasylnykov.com.l14_databaseandsharedprefexample.model.engines;

import android.content.Context;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.model.Phone;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.db_wrappers.RecordUserInfoDBWrapper;

public class RecordUserInfoEngine {

    private Context m_context = null;

    public RecordUserInfoEngine(Context context){
        m_context = context;
    }

    public ArrayList<RecordUserInfo> getAll(){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        ArrayList<RecordUserInfo> arrData = wrapper.getAll();
        PhoneEngine phoneEngine = new PhoneEngine(m_context);

        for(int i=0; i<arrData.size();i++){
            RecordUserInfo info = arrData.get(i);
            ArrayList<Phone> arrPhone = phoneEngine.getItemByUserId(info.getId());
            info.setPhones(arrPhone);
        }
        return arrData;
    }

    public RecordUserInfo getItemById(long nId){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        RecordUserInfo info = wrapper.getItemById(nId);
        PhoneEngine phoneEngine = new PhoneEngine(m_context);
        info.setPhones(phoneEngine.getItemByUserId(info.getId()));
        return info;
    }

    public void insertItem(RecordUserInfo item){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        long nUserId = wrapper.insertItem(item);
        ArrayList<Phone> arrPhones = item.getPhones();
        if (arrPhones!=null){
            PhoneEngine phoneEngine = new PhoneEngine(m_context);
            for (Phone phone:arrPhones){
                phone.setUserId(nUserId);
                phoneEngine.insertItem(phone);
            }
        }
    }

    public void updateItem(RecordUserInfo item){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        wrapper.updateItem(item);
        ArrayList<Phone> arrPhones = item.getPhones();
        if (arrPhones!=null){
            PhoneEngine phoneEngine = new PhoneEngine(m_context);
            for (Phone phone:arrPhones){
                if (phone.getId()==-1){
                    phone.setUserId(item.getId());
                    phoneEngine.insertItem(phone);
                } else {
                    phoneEngine.updateItem(phone);
                }

            }
        }
    }

    public void deleteItem(RecordUserInfo item){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        wrapper.deleteItem(item);
        PhoneEngine phoneEngine = new PhoneEngine(m_context);
        phoneEngine.deleteItemByUserId(item.getId());
    }

    public void deleteAll(){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        wrapper.deleteAll();
        PhoneEngine phoneEngine = new PhoneEngine(m_context);
        phoneEngine.deleteAll();
    }
}
