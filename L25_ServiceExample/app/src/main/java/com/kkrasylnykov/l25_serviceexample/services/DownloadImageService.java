package com.kkrasylnykov.l25_serviceexample.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DownloadImageService extends Service {

    public static final String ACTION_DOWNLOAD = "com.kkrasylnykov.l25_serviceexample.services.DownloadImageService:ACTION_DOWNLOAD";

    public static final String KEY_ACTION_TYPE = "KEY_ACTION_TYPE";

    public static final int ACTION_TYPE_FINISH = 200;
    public static final int ACTION_TYPE_ERROR = 404;


    public static final String KEY_URL = "KEY_URL";
    public static final String KEY_FILE_PATH = "KEY_FILE_PATH";

    private ExecutorService m_ExecutorService = null;
    private ArrayList<String> m_arrData = null;

    public DownloadImageService(){
        m_ExecutorService = Executors.newFixedThreadPool(1);
        m_arrData = new ArrayList<>();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("devcppService", "onStartCommand");
        String strURL = "";
        String strFilePath = "";

        if(intent!=null){
            Bundle bundle = intent.getExtras();
            strURL = bundle.getString(KEY_URL);
            strFilePath = bundle.getString(KEY_FILE_PATH);
        }

        if (strURL.isEmpty() || strFilePath.isEmpty()){
            return startId;
        }
        if (m_arrData.contains(strFilePath)){
            return startId;
        }
        m_arrData.add(strFilePath);
        m_ExecutorService.execute(new DownloadRunnable(strURL, strFilePath));

        return startId;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private class DownloadRunnable implements Runnable{

        private String m_strUrl = null;
        private String m_strFilePath = null;

        public DownloadRunnable(String strUrl, String strFilePath){
            m_strUrl = strUrl;
            m_strFilePath = strFilePath;
        }

        @Override
        public void run() {
            try {
                URL url = new URL(m_strUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(1000);
                conn.setConnectTimeout(1000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();
                Log.d("devcpp", m_strFilePath + " -> response -> " + response);
                if (response!=200){
                    return;
                }
                int contentLength = conn.getContentLength();
                InputStream inputStream = conn.getInputStream();
                FileOutputStream outputStream = new FileOutputStream(m_strFilePath);

                Log.d("devcpp","Thread -> read -> start");
                int nSum = 0;
                int bytesRead = -1;
                byte[] buffer = new byte[4096];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    nSum +=bytesRead;
                    Log.d("devcpp",m_strFilePath + " -> Thread -> read -> " + ((float)nSum)/contentLength);

                    outputStream.write(buffer, 0, bytesRead);
                }
                Log.d("devcpp",m_strFilePath + " -> Thread -> read -> end");

                outputStream.close();
                inputStream.close();
                Log.d("devcpp",m_strFilePath + " ->Thread -> stop");
                Intent intent = new Intent();
                intent.setAction(ACTION_DOWNLOAD+m_strFilePath);
                intent.putExtra(KEY_ACTION_TYPE, ACTION_TYPE_FINISH);
                sendBroadcast(intent);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                Intent intent = new Intent();
                intent.setAction(ACTION_DOWNLOAD+m_strFilePath);
                intent.putExtra(KEY_ACTION_TYPE, ACTION_TYPE_ERROR);
                sendBroadcast(intent);
            } catch (IOException e) {
                e.printStackTrace();
                Intent intent = new Intent();
                intent.setAction(ACTION_DOWNLOAD+m_strFilePath);
                intent.putExtra(KEY_ACTION_TYPE, ACTION_TYPE_ERROR);
                sendBroadcast(intent);
                Log.e("devcpp","IOException -> " + e.getMessage());
            }

        }
    }
}
