package com.kkrasylnykov.l25_serviceexample.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ImageInfo {

    private String m_strName = "";
    private String m_url = "";

    public ImageInfo(String m_strName, String m_url) {
        this.m_strName = m_strName;
        this.m_url = m_url;
    }

    public ImageInfo(String strJSON) {
        try {
            JSONObject jsonObject = new JSONObject(strJSON);
            this.m_strName = jsonObject.getString("name");
            this.m_url = jsonObject.getString("url");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return m_strName;
    }

    public String getUrl() {
        return m_url;
    }

    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name",getName());
            jsonObject.put("url",getUrl());
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
