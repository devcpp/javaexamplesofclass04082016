package l4_setexample;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

public class L4_SetExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        HashSet<Integer> setData = new HashSet<>();
        
        Integer nVal = new Integer(0);
        do{
            System.out.println("Введите не отрицательное значение:");
            nVal = scan.nextInt();
            setData.add(nVal);
        } while (nVal>0);
        setData.remove(nVal);
        
        System.out.println("Размер: " + setData.size());
        
        System.out.println("Содержание: " + setData.contains(3));
        
        for(Integer nValForPrint:setData){
            System.out.print(nValForPrint + " ");
        }
        
        System.out.println();
        
        ArrayList<Integer> arrData = new ArrayList<>();
        arrData.add(new Integer(2));
        arrData.add(new Integer(1));
        arrData.add(new Integer(5));
        arrData.add(new Integer(1));
        arrData.add(new Integer(3));
        
        setData.clear();
        setData.addAll(arrData);
        
        for(Integer nValForPrint:setData){
            System.out.print(nValForPrint + " ");
        }
    }
    
}
