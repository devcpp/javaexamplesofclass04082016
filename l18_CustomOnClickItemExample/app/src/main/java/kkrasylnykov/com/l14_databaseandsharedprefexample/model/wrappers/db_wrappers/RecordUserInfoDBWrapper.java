package kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.db_wrappers;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class RecordUserInfoDBWrapper extends BaseDBWrapper {

    public RecordUserInfoDBWrapper(Context context) {
        super(context, DBConstants.TABLE_NAME);
    }

    public ArrayList<RecordUserInfo> getAll(){
        ArrayList<RecordUserInfo> arrData = new ArrayList<>();
        SQLiteDatabase db = getReadable();
        Cursor cursor = db.query(getTableName(), null, null, null, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        RecordUserInfo userInfo = new RecordUserInfo(cursor);
                        arrData.add(userInfo);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrData;
    }

    public RecordUserInfo getItemById(long nId){
        RecordUserInfo userInfo = null;
        SQLiteDatabase db = getReadable();

        String strSelection = DBConstants.TABLE_CONTACTS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(nId)};

        Cursor cursor = db.query(getTableName(), null,
                strSelection, arrSelectionArgs, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    userInfo = new RecordUserInfo(cursor);
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return userInfo;
    }

    public void insertItem(RecordUserInfo item){
        SQLiteDatabase db = getWritable();
        ContentValues values = item.getContentValues();
        db.insert(getTableName(),null,values);
        db.close();
    }

    public void updateItem(RecordUserInfo item){
        SQLiteDatabase db = getWritable();
        ContentValues values = item.getContentValues();
        String strSelection = DBConstants.TABLE_CONTACTS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};
        db.update(getTableName(),values,strSelection, arrSelectionArgs);
        db.close();
    }

    public void deleteItem(RecordUserInfo item){
        SQLiteDatabase db = getWritable();
        String strSelection = DBConstants.TABLE_CONTACTS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};
        db.delete(getTableName(),strSelection,arrSelectionArgs);
        db.close();
    }

    public void deleteAll(){
        SQLiteDatabase db = getWritable();
        db.delete(getTableName(),null,null);
        db.close();
    }

    public ArrayList<RecordUserInfo> getSearch(String strSearch){
        String strLocalSearch = "%" + strSearch + "%";
        ArrayList<RecordUserInfo> arrData = new ArrayList<>();
        SQLiteDatabase db = getReadable();
        StringBuilder selectionBuilder = new StringBuilder();
        selectionBuilder.append(DBConstants.TABLE_CONTACTS_FIELD_NAME )
                .append(" LIKE ? OR ")
                .append(DBConstants.TABLE_CONTACTS_FIELD_SNAME)
                .append(" LIKE ? OR ")
                .append(DBConstants.TABLE_CONTACTS_FIELD_PHONE)
                .append(" LIKE ?");
        String strSelection = selectionBuilder.toString();
        /*String strSelection = DBConstants.TABLE_CONTACTS_FIELD_NAME +
                " LIKE ? OR " + DBConstants.TABLE_CONTACTS_FIELD_SNAME +
                " LIKE ? OR " + DBConstants.TABLE_CONTACTS_FIELD_PHONE +
                " LIKE ?";*/
        String[] arrSelectData = {strLocalSearch,strLocalSearch,strLocalSearch};
        Cursor cursor = db.query(getTableName(), null,
                strSelection, arrSelectData, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        RecordUserInfo userInfo = new RecordUserInfo(cursor);
                        arrData.add(userInfo);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrData;
    }


}
