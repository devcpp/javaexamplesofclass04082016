package kkrasylnykov.com.l14_databaseandsharedprefexample.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.R;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.extra_items.ItemMainList;

public class RecordUserInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<ItemMainList> m_arrData = null;
    private OnClickItemRecordInfo m_OnClickItemRecordInfo = null;

    public RecordUserInfoAdapter(ArrayList<ItemMainList> arrData){
        m_arrData = arrData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder result = null;
        if(viewType==ItemMainList.TYPE_RECORD){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_record_user_info, parent, false);
            result = new RecordInNotepadHolder(v);
        } else if (viewType==ItemMainList.TYPE_HEADER){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header_main, parent, false);
            result = new HeaderHolder(v);
        } else if (viewType==ItemMainList.TYPE_BOTTOM){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bottom_main, parent, false);
            result = new BottomHolder(v);
        }
        return result;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position)==ItemMainList.TYPE_RECORD){
            RecordUserInfo info = (RecordUserInfo)m_arrData.get(position).getData();
            RecordInNotepadHolder recordInNotepadHolder = (RecordInNotepadHolder) holder;
            recordInNotepadHolder.superView.setTag(info.getId());
            recordInNotepadHolder.tvName.setText(info.getName());
            recordInNotepadHolder.tvSName.setText(info.getSName());
            recordInNotepadHolder.tvPhone.setText(info.getPhone());
        } else if (getItemViewType(position)==ItemMainList.TYPE_HEADER){
            String strTitle = (String)m_arrData.get(position).getData();
            HeaderHolder header = (HeaderHolder) holder;
            header.tvHeader.setText(strTitle);
        } else if (getItemViewType(position)==ItemMainList.TYPE_BOTTOM){
            Integer nCount = (Integer)m_arrData.get(position).getData();
            BottomHolder header = (BottomHolder) holder;
            header.tvBottom.setText("Size: " + nCount);
        }
    }

    @Override
    public int getItemCount() {
        return m_arrData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return m_arrData.get(position).getType();
    }

    public void setOnClickItemRecordInfo(OnClickItemRecordInfo onClickItemRecordInfo){
        m_OnClickItemRecordInfo = onClickItemRecordInfo;
    }

    private class HeaderHolder extends RecyclerView.ViewHolder{

        TextView tvHeader = null;

        public HeaderHolder(View itemView) {
            super(itemView);
            tvHeader = (TextView) itemView.findViewById(R.id.textViewHeader);

        }
    }

    private class BottomHolder extends RecyclerView.ViewHolder{

        TextView tvBottom = null;

        public BottomHolder(View itemView) {
            super(itemView);
            tvBottom = (TextView) itemView.findViewById(R.id.textViewBottom);
        }
    }

    private class RecordInNotepadHolder extends RecyclerView.ViewHolder{
        View superView = null;

        TextView tvName = null;
        TextView tvSName = null;
        TextView tvPhone = null;

        public RecordInNotepadHolder(View itemView) {
            super(itemView);
            superView = itemView;
            superView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (m_OnClickItemRecordInfo!=null){
                        m_OnClickItemRecordInfo.onClickRecordInfo((long) superView.getTag());
                    }
                }
            });
            tvName = (TextView) itemView.findViewById(R.id.nameTextViewItem);
            tvSName = (TextView) itemView.findViewById(R.id.snameTextViewItem);
            tvPhone = (TextView) itemView.findViewById(R.id.phoneTextViewItem);
        }
    }

    public interface OnClickItemRecordInfo{
        public void onClickRecordInfo(long nId);
    }
}
