package kkrasylnykov.com.l14_databaseandsharedprefexample.model.extra_items;

public class ItemMainList {
    public static final int TYPE_RECORD = 1;
    public static final int TYPE_HEADER = 2;
    public static final int TYPE_BOTTOM = 3;

    private int m_nType = TYPE_RECORD;
    private Object m_Data = null;

    public ItemMainList(int nType, Object data){
        m_nType = nType;
        m_Data = data;
    }

    public int getType() {
        return m_nType;
    }

    public Object getData() {
        return m_Data;
    }
}
