package kkrasylnykov.com.l14_databaseandsharedprefexample.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.R;
import kkrasylnykov.com.l14_databaseandsharedprefexample.adapters.RecordUserInfoAdapter;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.engines.RecordUserInfoEngine;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.extra_items.ItemMainList;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.AppSettings;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        RecordUserInfoAdapter.OnClickItemRecordInfo{

    //private ListView m_ListView = null;
    private RecordUserInfoAdapter m_adapter = null;
    private RecyclerView m_RecyclerView = null;
    private ArrayList<ItemMainList> m_arrData = new ArrayList<>();

    private String m_strSearchString = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText editText = (EditText) findViewById(R.id.SearchEditText);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                m_strSearchString = s.toString();
                updateList();
            }
        });

        m_RecyclerView = (RecyclerView) findViewById(R.id.RecyclerViewMainActivity);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        m_RecyclerView.setLayoutManager(layoutManager);
        m_adapter = new RecordUserInfoAdapter(m_arrData);
        m_adapter.setOnClickItemRecordInfo(this);
        m_RecyclerView.setAdapter(m_adapter);

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.getIsFirstStart()){
            appSettings.setIsFirstStart(false);
        }

        View btnAdd1 = findViewById(R.id.addButton1MainActivity);
        btnAdd1.setOnClickListener(this);

        View btnRemove1 = findViewById(R.id.removeButton1MainActivity);
        btnRemove1.setOnClickListener(this);

        View btnUpdate1 = findViewById(R.id.updateButton1MainActivity);
        btnUpdate1.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateList();
    }

    private void updateList(){
        RecordUserInfoEngine engine = new RecordUserInfoEngine(this);
        ArrayList<RecordUserInfo> arrData = null;
        if(m_strSearchString.isEmpty()){
            arrData = engine.getAll();
        } else {
            arrData = engine.getSearch(m_strSearchString);
        }


        m_arrData.clear();
        m_arrData.add(new ItemMainList(ItemMainList.TYPE_HEADER,"--TITLE--"));
        int i =0;
        for (RecordUserInfo info:arrData){
            m_arrData.add(new ItemMainList(ItemMainList.TYPE_HEADER,"Position: " + i++));
            m_arrData.add(new ItemMainList(ItemMainList.TYPE_RECORD,info));
        }
        m_arrData.add(new ItemMainList(ItemMainList.TYPE_BOTTOM,new Integer(arrData.size())));
        m_adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addButton1MainActivity:
                /*m_arrData.add(0,new RecordUserInfo("TestAdd", "TestREmove", "0954065738"));
                m_adapter.notifyItemInserted(0);
                m_RecyclerView.scrollToPosition(0);*/
                break;
            case R.id.removeButton1MainActivity:
                ArrayList<ItemMainList> arrRemoving = new ArrayList<>();
                for (ItemMainList item:m_arrData){
                    if (item.getType()==ItemMainList.TYPE_HEADER){
                        arrRemoving.add(item);
                    }
                }
                for(ItemMainList item:arrRemoving){
                    int nPosition = m_arrData.indexOf(item);
                    m_arrData.remove(item);
                    m_adapter.notifyItemRemoved(nPosition);
                }
                break;
            case R.id.updateButton1MainActivity:
                /*m_arrData.set(0, new RecordUserInfo("TestUpdate", "Test123", "09234"));
                m_adapter.notifyItemChanged(0);*/
                break;
        }
    }

    @Override
    public void onClickRecordInfo(long nId) {
        Intent intentEdit = new Intent(this, ComposeActivity.class);
        intentEdit.putExtra(ComposeActivity.EXTRA_KEY_ID, nId);
        startActivity(intentEdit);
    }
}
