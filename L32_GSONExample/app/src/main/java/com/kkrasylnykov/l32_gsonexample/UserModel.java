package com.kkrasylnykov.l32_gsonexample;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserModel {


    @SerializedName("id")
    @Expose(serialize = false)
    private long m_nId = 0;
    @SerializedName("server_id")
    private Long m_nServerId = null;
    @SerializedName("name")
    private String m_strName = "";
    @SerializedName("phones")
    private ArrayList<PhoneModel> m_arrPhone = null;

    public UserModel(long m_nId, String m_strName, ArrayList<PhoneModel> m_arrPhone) {
        this.m_nId = m_nId;
        this.m_strName = m_strName;
        this.m_arrPhone = m_arrPhone;
    }

    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public ArrayList<PhoneModel> getPhone() {
        return m_arrPhone;
    }

    public void setPhone(ArrayList<PhoneModel> m_arrPhone) {
        this.m_arrPhone = m_arrPhone;
    }

    public long getM_nServerId() {
        return m_nServerId;
    }
}
