package com.kkrasylnykov.l32_gsonexample;

import com.google.gson.annotations.SerializedName;

public class PhoneModel {

    @SerializedName("id")
    private long m_nId = 0;
    @SerializedName("user_phone")
    private String m_strPhone = "";

    public PhoneModel (long nId, String strPhone){
        m_nId = nId;
        m_strPhone = strPhone;
    }

    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public String getPhone() {
        return m_strPhone;
    }

    public void setPhone(String m_strPhone) {
        this.m_strPhone = m_strPhone;
    }
}
