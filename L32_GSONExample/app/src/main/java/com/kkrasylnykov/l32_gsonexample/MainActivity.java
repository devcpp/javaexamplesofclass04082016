package com.kkrasylnykov.l32_gsonexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<PhoneModel> arrPhones = new ArrayList<>();
        arrPhones.add(new PhoneModel(95, "56845"));
        arrPhones.add(new PhoneModel(195, "5684345"));
        arrPhones.add(new PhoneModel(456, "99568545"));
        UserModel user = new UserModel(45,"Test",arrPhones);

        GsonBuilder builder = new GsonBuilder();
        builder.
        Gson gson = builder.create();

        String strJson = gson.toJson(user);
        Log.d("decppGSON",strJson);

        String strInputJson = "{\"phones\":[{\"user_phone\":\"789\",\"id\":78},{\"user_phone\":\"123325\",\"id\":296},{\"user_phone\":\"485\",\"id\":86}],\"name\":\"Test_serv\",\"server_id\":345,\"id\":1745}";
        UserModel inUser = gson.fromJson(strInputJson,UserModel.class);

        Log.d("decppGSON",inUser.getName());
    }
}
