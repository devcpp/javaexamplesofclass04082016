package l4_arraylistexample2;

import java.util.ArrayList;
import java.util.Scanner;

public class L4_ArrayListExample2 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        ArrayList<Float> arrData = new ArrayList<Float>();
        
        Float fVal = new Float(0);
        do{
            System.out.println("Введите не отрицательное значение:");
            fVal = scan.nextFloat();
            arrData.add(fVal);
        } while (fVal>0);
        System.out.println("Размер1: " + arrData.size());
        
        arrData.remove(fVal);
        
        System.out.println("Размер2: " + arrData.size());
        
        System.out.println("Значение: " + arrData.get(2));
        for (Float fValForSum:arrData){
            System.out.print(fValForSum + " ");
        }
        
        System.out.println();
        int nPosDel = 2;
        arrData.remove(nPosDel);
        
        for (Float fValForSum:arrData){
            System.out.print(fValForSum + " ");
        }
        
        System.out.println();
        int nPosChange = 1;
        arrData.set(nPosChange, new Float(4.7));
        
        for (Float fValForSum:arrData){
            System.out.print(fValForSum + " ");
        }
    }
    
}
