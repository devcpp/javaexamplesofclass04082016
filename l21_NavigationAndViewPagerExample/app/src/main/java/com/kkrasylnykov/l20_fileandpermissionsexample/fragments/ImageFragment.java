package com.kkrasylnykov.l20_fileandpermissionsexample.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kkrasylnykov.l20_fileandpermissionsexample.R;

import java.io.File;


public class ImageFragment extends Fragment {

    private ImageView m_ImageView = null;
    private String m_strPath = "";
    private Bitmap m_Bitmap = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        m_ImageView = (ImageView) view.findViewById(R.id.ImageView);
        return view;
    }

    public void setImagePath(String strImagePath){
        m_strPath = strImagePath;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (m_Bitmap==null){
            File imgFile = new  File(m_strPath);
            if(imgFile.exists()){
                m_Bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            }
        }
        m_ImageView.setImageBitmap(m_Bitmap);

    }
}
