package com.kkrasylnykov.l20_fileandpermissionsexample.activites;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kkrasylnykov.l20_fileandpermissionsexample.R;
import com.kkrasylnykov.l20_fileandpermissionsexample.adapters.FileNameAdapter;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final int REQUEST_PERMISSION_ON_ATTACH_FILE = 1001;

    private ListView m_ListView = null;
    private FileNameAdapter m_Adapter = null;
    private ArrayList<String> m_arrData = new ArrayList<>();

    private Toolbar m_toolbar = null;

    private DrawerLayout m_DrawerLayout = null;
    private View m_NavigationDrawer = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_DrawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayoutMainActivity);
        m_NavigationDrawer = findViewById(R.id.NavigationDrawer);

        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_ON_ATTACH_FILE);
        } else {
            searchFiles();
        }

        m_toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(m_toolbar);

        getSupportActionBar().setTitle(R.string.main_activity_name);

        Drawable menuIconDrawable = ContextCompat.getDrawable(this, R.drawable.ic_menu_black);
        menuIconDrawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        m_toolbar.setNavigationIcon(menuIconDrawable);

        m_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this, "Test -> ", Toast.LENGTH_LONG).show();
                m_DrawerLayout.openDrawer(m_NavigationDrawer);
            }
        });

        //Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    }

    private void searchFiles(){
        m_ListView = (ListView) findViewById(R.id.listViewMainActivity);
        m_arrData = getFiles(Environment.getExternalStorageDirectory());
        ArrayList<String> arrNameFiles = new ArrayList<>();
        for(String strFile:m_arrData){
            int nPosition = strFile.lastIndexOf("/")+1;
            arrNameFiles.add(strFile.substring(nPosition));
        }
        m_Adapter = new FileNameAdapter(arrNameFiles);
        m_ListView.setAdapter(m_Adapter);
        m_ListView.setOnItemClickListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==REQUEST_PERMISSION_ON_ATTACH_FILE){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(MainActivity.this, "Приложение не может работать без разрешения", Toast.LENGTH_LONG).show();
                finish();
            } else {
                searchFiles();
            }
        }
    }

    private ArrayList<String> getFiles(File file){
        ArrayList<String> arrData = new ArrayList<>();
        if (file.isDirectory()){
            File[] files = file.listFiles();
            if (files!=null){
                for (File curFile:files){
                    arrData.addAll(getFiles(curFile));
                }
            }

        } else if (file.isFile()){

            if (file.getName().toLowerCase().contains(".jpg")){
                arrData.add(file.getAbsolutePath());
            }
        }
        return arrData;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String[] arrData = m_arrData.toArray(new String[0]);

        Intent intent = new Intent(this, ViewerActivity.class);
        intent.putExtra(ViewerActivity.KEY_POSITION, position);
        intent.putExtra(ViewerActivity.KEY_ARRAY_DATA,arrData);
        startActivity(intent);

    }
}
