package com.kkrasylnykov.l20_fileandpermissionsexample.activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.kkrasylnykov.l20_fileandpermissionsexample.R;
import com.kkrasylnykov.l20_fileandpermissionsexample.adapters.ImageFragmentViewPagerAdapter;


public class ViewerActivity extends AppCompatActivity {

    public static final String KEY_POSITION = "KEY_POSITION";
    public static final String KEY_ARRAY_DATA = "KEY_ARRAY_DATA";

    private ViewPager m_ViewPager = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer);

        int nStartPosition = 0;
        String[] arrData = null;

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                nStartPosition = bundle.getInt(KEY_POSITION,0);
                arrData = bundle.getStringArray(KEY_ARRAY_DATA);
            }
        }

        m_ViewPager = (ViewPager) findViewById(R.id.viewPagerViewerActivity);
        ImageFragmentViewPagerAdapter adapter = new ImageFragmentViewPagerAdapter(getSupportFragmentManager(), arrData);
        m_ViewPager.setAdapter(adapter);
        m_ViewPager.setCurrentItem(nStartPosition);

    }
}
