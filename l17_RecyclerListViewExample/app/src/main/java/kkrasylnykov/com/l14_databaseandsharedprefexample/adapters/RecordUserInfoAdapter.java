package kkrasylnykov.com.l14_databaseandsharedprefexample.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.R;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;

public class RecordUserInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_RECORD = 1;

    ArrayList<RecordUserInfo> m_arrData = null;

    public RecordUserInfoAdapter(ArrayList<RecordUserInfo> arrData){
        m_arrData = arrData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder result = null;
        if(viewType==TYPE_RECORD){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_record_user_info, parent, false);
            result = new RecordInNotepadHolder(v);
        }
        return result;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position)==TYPE_RECORD){
            RecordUserInfo info = m_arrData.get(position);
            RecordInNotepadHolder recordInNotepadHolder = (RecordInNotepadHolder) holder;
            recordInNotepadHolder.tvName.setText(info.getName());
            recordInNotepadHolder.tvSName.setText(info.getSName());
            recordInNotepadHolder.tvPhone.setText(info.getPhone());
        }
    }

    @Override
    public int getItemCount() {
        return m_arrData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_RECORD;
    }

    private class RecordInNotepadHolder extends RecyclerView.ViewHolder{

        TextView tvName = null;
        TextView tvSName = null;
        TextView tvPhone = null;

        public RecordInNotepadHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.nameTextViewItem);
            tvSName = (TextView) itemView.findViewById(R.id.snameTextViewItem);
            tvPhone = (TextView) itemView.findViewById(R.id.phoneTextViewItem);
        }
    }
}
