package kkrasylnykov.com.l14_databaseandsharedprefexample.model.engines;

import android.content.Context;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.db_wrappers.RecordUserInfoDBWrapper;

public class RecordUserInfoEngine {

    private Context m_context = null;

    public RecordUserInfoEngine(Context context){
        m_context = context;
    }

    public ArrayList<RecordUserInfo> getAll(){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        return wrapper.getAll();
    }

    public RecordUserInfo getItemById(long nId){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        return wrapper.getItemById(nId);
    }

    public void insertItem(RecordUserInfo item){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        wrapper.insertItem(item);
    }

    public void updateItem(RecordUserInfo item){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        wrapper.updateItem(item);
    }

    public void deleteItem(RecordUserInfo item){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        wrapper.deleteItem(item);
    }

    public void deleteAll(){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        wrapper.deleteAll();
    }

    public ArrayList<RecordUserInfo> getSearch(String strSearch){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        return wrapper.getSearch(strSearch);
    }
}
