package kkrasylnykov.com.l14_databaseandsharedprefexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import kkrasylnykov.com.l14_databaseandsharedprefexample.R;
import kkrasylnykov.com.l14_databaseandsharedprefexample.db.DBHelper;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.engines.RecordUserInfoEngine;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;


public class ComposeActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_KEY_ID = "EXTRA_KEY_ID";

    /*private long m_nId = -1;*/
    private RecordUserInfo m_userInfo = null;

    private EditText m_nameEditText;
    private EditText m_snameEditText;
    private EditText m_phoneEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose);

        Button btnAdd = (Button) findViewById(R.id.addButtonComposeActivity);
        btnAdd.setOnClickListener(this);

        Button btnRemove = (Button) findViewById(R.id.removeButtonComposeActivity);
        btnRemove.setOnClickListener(this);

        m_nameEditText = (EditText) findViewById(R.id.nameEditTextComposeActivity);
        m_snameEditText = (EditText) findViewById(R.id.snameEditTextComposeActivity);
        m_phoneEditText = (EditText) findViewById(R.id.phoneEditTextComposeActivity);


        Intent intent = getIntent();
        long nId = -1;
        if(intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                nId = bundle.getLong(EXTRA_KEY_ID,-1);
            }
        }

        if (nId!=-1){
            btnAdd.setText("Update");

            btnRemove.setVisibility(View.VISIBLE);

            RecordUserInfoEngine engine = new RecordUserInfoEngine(this);
            m_userInfo = engine.getItemById(nId);
            m_nameEditText.setText(m_userInfo.getName());
            m_snameEditText.setText(m_userInfo.getSName());
            m_phoneEditText.setText(m_userInfo.getPhone());
        }
    }

    @Override
    public void onClick(View v) {
        RecordUserInfoEngine engine = new RecordUserInfoEngine(this);
        switch (v.getId()){
            case R.id.addButtonComposeActivity:
                if (m_userInfo==null){
                    m_userInfo = new RecordUserInfo(m_nameEditText.getText().toString(),
                            m_snameEditText.getText().toString(),
                            m_phoneEditText.getText().toString());
                } else {
                    m_userInfo.setName(m_nameEditText.getText().toString());
                    m_userInfo.setSName(m_snameEditText.getText().toString());
                    m_userInfo.setPhone(m_phoneEditText.getText().toString());
                }

                if(m_userInfo.getId()!=-1){
                    engine.updateItem(m_userInfo);
                } else {
                    engine.insertItem(m_userInfo);
                }
                break;

            case R.id.removeButtonComposeActivity:
                engine.deleteItem(m_userInfo);
                break;
        }
        finish();
    }
}
