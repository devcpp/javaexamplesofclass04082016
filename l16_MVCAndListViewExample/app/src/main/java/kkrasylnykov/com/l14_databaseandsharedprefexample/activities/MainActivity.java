package kkrasylnykov.com.l14_databaseandsharedprefexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.R;
import kkrasylnykov.com.l14_databaseandsharedprefexample.adapters.RecordUserInfoAdapter;
import kkrasylnykov.com.l14_databaseandsharedprefexample.db.DBHelper;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.engines.RecordUserInfoEngine;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.AppSettings;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private ListView m_ListView = null;
    private RecordUserInfoAdapter m_adapter = null;
    private ArrayList<RecordUserInfo> m_arrData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_ListView = (ListView) findViewById(R.id.listViewMainActivity);
        m_adapter = new RecordUserInfoAdapter(m_arrData);
        m_ListView.setAdapter(m_adapter);
        m_ListView.setOnItemClickListener(this);
        View btnAdd = findViewById(R.id.addButtonMainActivity);
        btnAdd.setOnClickListener(this);

        View btnRemoveAll = findViewById(R.id.removeAllButtonMainActivity);
        btnRemoveAll.setOnClickListener(this);

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.getIsFirstStart()){
            appSettings.setIsFirstStart(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateList();
    }

    private void updateList(){
        RecordUserInfoEngine engine = new RecordUserInfoEngine(this);
        ArrayList<RecordUserInfo> arrData = engine.getAll();

        m_arrData.clear();
        m_arrData.addAll(arrData);
        m_adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addButtonMainActivity:
                Intent intent = new Intent(this, ComposeActivity.class);
                startActivity(intent);
                break;
            case R.id.removeAllButtonMainActivity:
                RecordUserInfoEngine engine = new RecordUserInfoEngine(this);
                engine.deleteAll();
                updateList();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intentEdit = new Intent(this, ComposeActivity.class);
        intentEdit.putExtra(ComposeActivity.EXTRA_KEY_ID, id);
        startActivity(intentEdit);
    }
}
