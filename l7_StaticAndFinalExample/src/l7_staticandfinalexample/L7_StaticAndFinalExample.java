package l7_staticandfinalexample;

import java.util.Scanner;

public class L7_StaticAndFinalExample {

    public static void main(String[] args) {
        //StaticClasss.s_nCount = 200;
        //System.out.println("StaticClasss.getCount -> " + StaticClasss.MAX_COUNT);
        //StaticClasss sc1 = new StaticClasss();
        
        /*StaticClasss sc2 = new StaticClasss();
        StaticClasss sc3 = new StaticClasss();
        StaticClasss sc4 = new StaticClasss();*/
        
        int[] arrData = {1,2,5,7,8,10};
        
        Scanner scan = new Scanner(System.in);
        try{
            String strVal1 = scan.nextLine();
            int nVal1 = Integer.valueOf(strVal1);
            
            String strVal2 = scan.nextLine();
            int nVal2 = Integer.valueOf(strVal2);
            
            int nVal3 = arrData[nVal2];
            
            int nVal4 = nVal3/nVal1;
        } catch(NumberFormatException e){
            System.out.println("Вы ввели не число!!!! -> " + e.getMessage());
        } catch(IndexOutOfBoundsException e){
            System.out.println("Вы вышли за пределы массива!!!");
        } catch(ArithmeticException e){
            System.out.println("Делить на ноль - нельзя!!!!!!");
        } catch(Exception e){
            System.out.println("Неизвестная ошибка!! -> " + e.toString());
        } finally {
            System.out.println("Этот блок выполняется в любом случае!");
        }
    }
    
}
