package l7_staticandfinalexample;

import java.util.Scanner;

public class StaticClasss {
    
    public final static int MAX_COUNT = 100;
    
    public static int s_nCount = 0;
    private int m_nCount = 0;
    
    
    public StaticClasss(){
        s_nCount++;
        m_nCount++;
        System.out.println("s_nCount -> " + s_nCount);
        System.out.println("m_nCount -> " + m_nCount);
    }
    
    public static int getCount(){
        return s_nCount;
    }
}
