package kkrasylnykov.com.l13_customviewexample.custom_view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kkrasylnykov.com.l13_customviewexample.R;

public class CompositImageAndTextView extends RelativeLayout {

    private TextView m_tv = null;
    private ImageView m_iv = null;
    private boolean m_bIsStartCalc = true;

    public CompositImageAndTextView(Context context) {
        super(context);
        init(context);
    }

    public CompositImageAndTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CompositImageAndTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        LayoutInflater.from(context).inflate(R.layout.custom_view_composit_image_and_text_view,this,true);
        m_tv = (TextView) findViewById(R.id.compositeTextView);
        m_iv = (ImageView) findViewById(R.id.compositeImageView);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (m_bIsStartCalc){
            m_bIsStartCalc = false;
            Log.d("devcppMeasureSpec" , "m_bIsStartCalc1");
            int nRealWidth = MeasureSpec.getSize(widthMeasureSpec);

            int nStartMargin = m_iv.getMeasuredWidth() + 10;
            RelativeLayout.LayoutParams params =
                    (RelativeLayout.LayoutParams)m_tv.getLayoutParams();
            params.setMargins(nStartMargin,0,0,0);

            int nTextVieWidthMeasureSpec = MeasureSpec.makeMeasureSpec((nRealWidth - nStartMargin),MeasureSpec.AT_MOST);
            m_tv.measure(nTextVieWidthMeasureSpec, MeasureSpec.UNSPECIFIED);
            float fTextSize = m_tv.getTextSize();
            while(m_tv.getMeasuredHeight()>m_iv.getMeasuredHeight()){
                fTextSize-=1;
                m_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX,fTextSize);
                m_tv.measure(nTextVieWidthMeasureSpec, MeasureSpec.UNSPECIFIED);
            }
            /*float fDelta = (float) m_iv.getMeasuredHeight()/m_tv.getMeasuredHeight();
            float fTextSize = m_tv.getTextSize() * fDelta;
            Log.d("devcppMeasureSpec" , "m_tv.getMeasuredHeight() -> " + m_tv.getMeasuredHeight());
            Log.d("devcppMeasureSpec" , "m_iv.getMeasuredHeight() -> " + m_iv.getMeasuredHeight());
            Log.d("devcppMeasureSpec" , "fDelta -> " + fDelta);
            Log.d("devcppMeasureSpec" , "fTextSize -> " + fTextSize);
            m_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX,fTextSize);*/
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
