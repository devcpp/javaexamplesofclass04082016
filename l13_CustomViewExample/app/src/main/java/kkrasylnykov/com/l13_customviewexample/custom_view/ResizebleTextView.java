package kkrasylnykov.com.l13_customviewexample.custom_view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import kkrasylnykov.com.l13_customviewexample.R;

public class ResizebleTextView extends LinearLayout {

    private TextView m_tw = null;
    private boolean m_bIsStartCalc = true;


    public ResizebleTextView(Context context) {
        super(context);
        init(context);
    }

    public ResizebleTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ResizebleTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        LayoutInflater.from(context).inflate(R.layout.custom_view_resizeble_text_view,this,true);
        m_tw = (TextView) findViewById(R.id.resizeTextView);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (m_bIsStartCalc){
            m_bIsStartCalc = false;
            int nRealWidth = MeasureSpec.getSize(widthMeasureSpec);
            int nRealHeight = MeasureSpec.getSize(heightMeasureSpec);
            m_tw.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
            int nTextViewWidth = m_tw.getMeasuredWidth();
            Log.d("devcppMeasureSpec","nRealWidth -> " + nRealWidth);
            Log.d("devcppMeasureSpec","nTextViewWidth -> " + nTextViewWidth);
            float fDelta = (float)nRealWidth/nTextViewWidth;
            Log.d("devcppMeasureSpec","fDelta -> " + fDelta);
            float fTextSize = m_tw.getTextSize() * fDelta;
            Log.d("devcppMeasureSpec","m_tw.getTextSize() -> " + m_tw.getTextSize());
            Log.d("devcppMeasureSpec","fTextSize -> " + fTextSize);
            m_tw.setTextSize(TypedValue.COMPLEX_UNIT_PX,fTextSize);
            while(m_tw.getMeasuredWidth()>nRealWidth){
                fTextSize-=1;
                m_tw.setTextSize(TypedValue.COMPLEX_UNIT_PX,fTextSize);
                m_tw.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
            }

            int nTopMargin = (nRealHeight - m_tw.getMeasuredHeight())/2;
            LinearLayout.LayoutParams params =
                    (LinearLayout.LayoutParams)m_tw.getLayoutParams();
            params.setMargins(0,nTopMargin,0,0);

        }


    }
}
