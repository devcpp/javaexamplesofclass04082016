package kkrasylnykov.com.l13_customviewexample.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import kkrasylnykov.com.l13_customviewexample.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
