package com.kkrasylnykov.l20_fileandpermissionsexample.activites;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kkrasylnykov.l20_fileandpermissionsexample.R;
import com.kkrasylnykov.l20_fileandpermissionsexample.adapters.FileNameAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private  static  final int MENU_ITEM_TIMER = 101;
    private  static  final int MENU_ITEM_DEL = 102;
    private  static  final int MENU_ITEM_ROBOT = 103;
    private  static  final int MENU_ITEM_OPEN = 104;
    private  static  final int MENU_ITEM_HELP = 105;

    private static final int REQUEST_PERMISSION_ON_ATTACH_FILE = 1001;

    private ListView m_ListView = null;
    private FileNameAdapter m_Adapter = null;
    private ArrayList<String> m_arrData = new ArrayList<>();

    private Toolbar m_toolbar = null;

    private DrawerLayout m_DrawerLayout = null;
    private View m_NavigationDrawer = null;

    private long nBank = 1000;

    private long m_nArg1 = 0;
    private long m_nArg2 = 0;
    private long m_nArg3 = 0;
    private long m_nSum = 0;

    private CountDownLatch m_CountDownLatch = null;

    private Thread m_ThreadForStop = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_DrawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayoutMainActivity);
        m_NavigationDrawer = findViewById(R.id.NavigationDrawer);

        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_ON_ATTACH_FILE);
        } else {
            searchFiles();
        }

        m_toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(m_toolbar);

        getSupportActionBar().setTitle(R.string.main_activity_name);

        Drawable menuIconDrawable = ContextCompat.getDrawable(this, R.drawable.ic_menu_black);
        menuIconDrawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        m_toolbar.setNavigationIcon(menuIconDrawable);

        Drawable menuRightIconDrawable = ContextCompat.getDrawable(this, R.drawable.ic_more_vert);
        menuRightIconDrawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        m_toolbar.setOverflowIcon(menuRightIconDrawable);


        m_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this, "Test -> ", Toast.LENGTH_LONG).show();
                m_DrawerLayout.openDrawer(m_NavigationDrawer);
            }
        });

        //Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    }

    private void searchFiles(){
        m_ListView = (ListView) findViewById(R.id.listViewMainActivity);
        m_arrData = getFiles(Environment.getExternalStorageDirectory());
        ArrayList<String> arrNameFiles = new ArrayList<>();
        for(String strFile:m_arrData){
            int nPosition = strFile.lastIndexOf("/")+1;
            arrNameFiles.add(strFile.substring(nPosition));
        }
        m_Adapter = new FileNameAdapter(arrNameFiles);
        m_ListView.setAdapter(m_Adapter);
        m_ListView.setOnItemClickListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==REQUEST_PERMISSION_ON_ATTACH_FILE){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(MainActivity.this, "Приложение не может работать без разрешения", Toast.LENGTH_LONG).show();
                finish();
            } else {
                searchFiles();
            }
        }
    }

    private ArrayList<String> getFiles(File file){
        ArrayList<String> arrData = new ArrayList<>();
        if (file.isDirectory()){
            File[] files = file.listFiles();
            if (files!=null){
                for (File curFile:files){
                    arrData.addAll(getFiles(curFile));
                }
            }

        } else if (file.isFile()){

            if (file.getName().toLowerCase().contains(".jpg")){
                arrData.add(file.getAbsolutePath());
            }
        }
        return arrData;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String[] arrData = m_arrData.toArray(new String[0]);

        Intent intent = new Intent(this, ViewerActivity.class);
        intent.putExtra(ViewerActivity.KEY_POSITION, position);
        intent.putExtra(ViewerActivity.KEY_ARRAY_DATA,arrData);
        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem itemTime = menu.add(0, MENU_ITEM_TIMER,0,"Timer");
        Drawable drawableIconTimer = ContextCompat.getDrawable(this, R.drawable.ic_alarm);
        drawableIconTimer.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemTime.setIcon(drawableIconTimer);
        itemTime.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem itemDel = menu.add(0, MENU_ITEM_DEL,1,"Delete");
        Drawable drawableIconDel = ContextCompat.getDrawable(this, R.drawable.ic_delete);
        drawableIconDel.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemDel.setIcon(drawableIconDel);
        itemDel.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem itemRobot = menu.add(0, MENU_ITEM_ROBOT,2,"Robot");
        Drawable drawableIconRobot = ContextCompat.getDrawable(this, R.drawable.ic_android);
        drawableIconRobot.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemRobot.setIcon(drawableIconRobot);
        itemRobot.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        MenuItem itemOpen = menu.add(0, MENU_ITEM_OPEN,3,"Open");
        Drawable drawableIconOpen = ContextCompat.getDrawable(this, R.drawable.ic_open);
        drawableIconOpen.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemOpen.setIcon(drawableIconOpen);
        itemOpen.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        MenuItem itemHelp = menu.add(0, MENU_ITEM_HELP,4,"Help");
        itemHelp.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case MENU_ITEM_TIMER:
                Thread increment = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0;i<500000; i++){
                            addToBank();

                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, "nBank+ -> " + nBank, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
                Thread decrement = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0;i<500000; i++){
                            removeToBank();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, "nBank- -> " + nBank, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
                increment.start();
                decrement.start();
                break;
            case MENU_ITEM_DEL:
                m_ThreadForStop.interrupt();
                break;
            case MENU_ITEM_ROBOT:
                Runnable sumRunnable = new Runnable() {
                    @Override
                    public void run() {
                        long nSum = 0;
                        for (long i =0; i<1000000001; i++){
                            nSum+=i;
                        }
                        final long nResult = nSum;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, "nSum -> " + nResult, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                };
                Thread sumThread = new Thread(sumRunnable);
                sumThread.start();

                break;
            case MENU_ITEM_OPEN:
                m_CountDownLatch = new CountDownLatch(2);
                Thread math1 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        m_nArg1 = 25;
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        try {
                            m_CountDownLatch.await();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        m_nSum = m_nArg1 + m_nArg2 + m_nArg3;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, "m_nSum -> " + m_nSum, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
                Thread math2 = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        m_nArg2 = -35;
                        m_CountDownLatch.countDown();
                    }
                });
                Thread math3 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(4500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        m_nArg3 = 105;
                        m_CountDownLatch.countDown();
                    }
                });
                math1.start();
                math2.start();
                math3.start();

                break;
            case MENU_ITEM_HELP:
                Runnable sumRunnable2 = new Runnable() {
                    @Override
                    public void run() {
                        long nSum = 0;
                        for (long i =0; i<500000001; i++){
                            if (m_ThreadForStop.isInterrupted()){
                                return;
                            }
                            nSum+=i;
                            if (i%1000==0){
                                Log.d("devcppl23", "i -> " + i + " <<>> " + m_ThreadForStop.isInterrupted());
                            }

                        }
                        final long nResult = nSum;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, "nSum -> " + nResult, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                };
                m_ThreadForStop = new Thread(sumRunnable2);
                m_ThreadForStop.start();
                break;
        }
        return false;
    }

    private synchronized void addToBank(){
        nBank++;
    }

    private synchronized void removeToBank(){
        nBank--;
    }
}
