package com.kkrasylnykov.l20_fileandpermissionsexample.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.kkrasylnykov.l20_fileandpermissionsexample.fragments.ImageFragment;

public class ImageFragmentViewPagerAdapter extends FragmentPagerAdapter {

    private String[] m_arrData = null;

    public ImageFragmentViewPagerAdapter(FragmentManager fm, String[] arrData) {
        super(fm);
        m_arrData = arrData;
    }

    @Override
    public Fragment getItem(int position) {
        ImageFragment fragment = new ImageFragment();
        fragment.setImagePath(m_arrData[position]);
        return fragment;
    }

    @Override
    public int getCount() {
        return m_arrData.length;
    }
}
