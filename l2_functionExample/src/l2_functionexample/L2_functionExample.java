package l2_functionexample;

import java.util.Scanner;

public class L2_functionExample {
    
    public static int pow(int nVal, int nPow){
        int nResult = -1;
        if (nPow>=0){
            if (nPow==0){
                nResult = 1;
            } else {
                nResult = nVal*pow(nVal,nPow-1);
            }
        }
        
        
        return nResult;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int nVal1 = 0;
        int nVal2 = 0;
        int nVal3 = 0;
        
        nVal1 = scan.nextInt();
        int nRes1 = pow(nVal1, 2);
        
        nVal2 = scan.nextInt();
        int nRes2 = pow(nVal2, 3);
        
        nVal3 = scan.nextInt();
        int nRes3 = pow(nVal3, 4);
        
        System.out.println("nRes1 -> " + nRes1);
        System.out.println("nRes2 -> " + nRes2);
        System.out.println("nRes3 -> " + nRes3);
    }
    
}
