package l4_arraylistexample;

import java.util.ArrayList;
import java.util.Scanner;

public class L4_ArrayListExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        ArrayList<Float> arrData = new ArrayList<Float>();
        
        Float fVal = new Float(0);
        do{
            System.out.println("Введите не отрицательное значение:");
            fVal = scan.nextFloat();
            arrData.add(fVal);
        } while (fVal>0);
        arrData.remove(fVal);
        
        float fSum = 0;
        for (Float fValForSum:arrData){
            fSum += fValForSum;
        }
        
        System.out.println("Cумма: " + fSum);
        
        do{
            System.out.println("Введите не отрицательное значение:");
            fVal = scan.nextFloat();
            int nPosition = arrData.indexOf(fVal);
            if (nPosition>-1){
                System.out.println("Позиция: " + nPosition);
            } else {
                System.err.println("Такого значения в массиве нет");
            }
        } while (fVal>0);
        
        ArrayList<Float> arrDataForExample = new ArrayList<>();
        arrDataForExample.addAll(arrData);
        
    }
    
}
