package com.kkrasylnykov.l20_fileandpermissionsexample.adapter;

import android.app.ActionBar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class FileNameAdapter extends BaseAdapter {

    private ArrayList<String> m_arrData = null;

    public FileNameAdapter(ArrayList<String> arrData){
        m_arrData = arrData;
    }

    @Override
    public int getCount() {
        return m_arrData.size();
    }

    @Override
    public Object getItem(int position) {
        return m_arrData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView = new TextView(parent.getContext());
            convertView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.MATCH_PARENT));
        }
        ((TextView)convertView).setText(m_arrData.get(position));
        return convertView;
    }
}
