package com.kkrasylnykov.l20_fileandpermissionsexample.activites;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.kkrasylnykov.l20_fileandpermissionsexample.R;
import com.kkrasylnykov.l20_fileandpermissionsexample.adapter.FileNameAdapter;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSION_ON_ATTACH_FILE = 1001;

    private ListView m_ListView = null;
    private FileNameAdapter m_Adapter = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_ON_ATTACH_FILE);
        } else {
            searchFiles();
        }
        //Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    }

    private void searchFiles(){
        m_ListView = (ListView) findViewById(R.id.listViewMainActivity);
        ArrayList<String> arrData = getFiles(Environment.getExternalStorageDirectory());
        ArrayList<String> arrNameFiles = new ArrayList<>();
        for(String strFile:arrData){
            int nPosition = strFile.lastIndexOf("/")+1;
            arrNameFiles.add(strFile.substring(nPosition));
        }
        m_Adapter = new FileNameAdapter(arrNameFiles);
        m_ListView.setAdapter(m_Adapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==REQUEST_PERMISSION_ON_ATTACH_FILE){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(MainActivity.this, "Приложение не может работать без разрешения", Toast.LENGTH_LONG).show();
                finish();
            } else {
                searchFiles();
            }
        }
    }

    private ArrayList<String> getFiles(File file){
        ArrayList<String> arrData = new ArrayList<>();
        if (file.isDirectory()){
            File[] files = file.listFiles();
            if (files!=null){
                for (File curFile:files){
                    arrData.addAll(getFiles(curFile));
                }
            }

        } else if (file.isFile()){

            if (file.getName().toLowerCase().contains(".jpg")){
                arrData.add(file.getAbsolutePath());
            }
        }
        return arrData;
    }
}
