package kkrasylnykov.com.l12_orientationfragmentexample.activities;

import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.widget.TextView;

import kkrasylnykov.com.l12_orientationfragmentexample.R;
import kkrasylnykov.com.l12_orientationfragmentexample.fragments.ListFragment;
import kkrasylnykov.com.l12_orientationfragmentexample.fragments.ViewerFragment;

public class MainActivity extends AppCompatActivity {

    private ListFragment m_list = null;
    private ViewerFragment m_viewer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_list = new ListFragment();
        m_viewer = new ViewerFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if (getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){
            transaction.add(R.id.firstFrameLayoutMainActivity, m_list);
            transaction.add(R.id.secondFrameLayoutMainActivity, m_viewer);
        } else {
            transaction.replace(R.id.firstFrameLayoutMainActivity, m_list);
        }
        transaction.commit();
    }

    public void setText(String strText){
        m_viewer.setTextForView(strText);
        if (getResources().getConfiguration().orientation!=Configuration.ORIENTATION_LANDSCAPE){
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.firstFrameLayoutMainActivity, m_viewer);
            transaction.commit();
        }
    }
}
