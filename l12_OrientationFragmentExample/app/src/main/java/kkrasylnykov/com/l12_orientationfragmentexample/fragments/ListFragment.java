package kkrasylnykov.com.l12_orientationfragmentexample.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import kkrasylnykov.com.l12_orientationfragmentexample.R;
import kkrasylnykov.com.l12_orientationfragmentexample.activities.MainActivity;

public class ListFragment extends Fragment implements View.OnClickListener {
    private Button m_btn1 = null;
    private Button m_btn2 = null;
    private Button m_btn3 = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        m_btn1 = (Button) view.findViewById(R.id.button1);
        m_btn2 = (Button) view.findViewById(R.id.button2);
        m_btn3 = (Button) view.findViewById(R.id.button3);
        m_btn1.setOnClickListener(this);
        m_btn2.setOnClickListener(this);
        m_btn3.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        Button btn = (Button)view;
        String strText = btn.getText().toString();
        ((MainActivity)getActivity()).setText(strText);
    }
}
