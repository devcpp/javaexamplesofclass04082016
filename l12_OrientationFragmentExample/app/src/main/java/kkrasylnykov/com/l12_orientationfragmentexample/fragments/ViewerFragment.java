package kkrasylnykov.com.l12_orientationfragmentexample.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import kkrasylnykov.com.l12_orientationfragmentexample.R;

public class ViewerFragment extends Fragment {

    private TextView m_TextView = null;
    private String m_strText = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_viewer, container, false);
        m_TextView = (TextView) view.findViewById(R.id.TextView);
        m_TextView.setText(m_strText);
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        m_TextView = null;
    }

    public void setTextForView(String strText){
        if (m_TextView!=null){
            m_TextView.setText(strText);
        } else {
            m_strText = strText;
        }
    }
}
