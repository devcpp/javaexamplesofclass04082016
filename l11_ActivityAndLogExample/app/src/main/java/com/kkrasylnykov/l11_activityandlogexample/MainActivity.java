package com.kkrasylnykov.l11_activityandlogexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final int REQUEST_CODE_SHOW_INFO = 235;

    private EditText m_NameEditText = null;
    private EditText m_SNameEditText = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View testClick = findViewById(R.id.testClickButton);
        testClick.setOnClickListener(this);

        m_NameEditText = (EditText) findViewById(R.id.userNameEditText);
        m_SNameEditText = (EditText) findViewById(R.id.userSNameEditText);

        Log.d("devcpp","onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp","onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp","onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("devcpp","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("devcpp","onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("devcpp","onDestroy");
    }

    @Override
    public void onClick(View view) {
        Log.d("devcpp","onClick -> " + view.getId());
        String strName = m_NameEditText.getText().toString();
        String strSName = m_SNameEditText.getText().toString();
        Intent intent = new Intent(MainActivity.this,ShowInfoActivity.class);
        intent.putExtra(ShowInfoActivity.EXTRA_KEY_STRING_NAME,strName);
        intent.putExtra(ShowInfoActivity.EXTRA_KEY_STRING_SNAME,strSName);
        startActivityForResult(intent,REQUEST_CODE_SHOW_INFO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("devcpp","onActivityResult -> requestCode ->" + requestCode);
        if(requestCode==REQUEST_CODE_SHOW_INFO){
            if (resultCode==RESULT_OK){
                if (data!=null){
                    Bundle bundle = data.getExtras();
                    if(bundle!=null){
                        String strText = bundle.getString(ShowInfoActivity.EXTRA_KEY_STRING_TEXT, "");
                        Log.d("devcpp","onActivityResult -> data -> " + strText);
                    }
                }
            } else if (resultCode==RESULT_CANCELED){
                Log.d("devcpp","onActivityResult -> resultCode -> RESULT_CANCELED");
            }

        }
    }
}
