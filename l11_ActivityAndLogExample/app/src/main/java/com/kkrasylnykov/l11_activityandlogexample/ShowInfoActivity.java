package com.kkrasylnykov.l11_activityandlogexample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ShowInfoActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_KEY_STRING_NAME = "EXTRA_KEY_STRING_NAME";
    public static final String EXTRA_KEY_STRING_SNAME = "EXTRA_KEY_STRING_SNAME";

    public static final String EXTRA_KEY_STRING_TEXT = "EXTRA_KEY_STRING_TEXT";

    private EditText m_textEditText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_info);

        String strName = "";
        String strSName = "";

        Intent intent = getIntent();
        if(intent!=null){
            Bundle bundle = intent.getExtras();
            if(bundle!=null){
                strName = bundle.getString(EXTRA_KEY_STRING_NAME, "");
                strSName = bundle.getString(EXTRA_KEY_STRING_SNAME, "");
            }
        }

        TextView nameTextView = (TextView)findViewById(R.id.nameShowInfoActivityTextView);
        nameTextView.setText(strName + " " + strSName);

        View yesButton = findViewById(R.id.yesShowInfoActivityButton);
        yesButton.setOnClickListener(this);
        View noButton = findViewById(R.id.noShowInfoActivityButton);
        noButton.setOnClickListener(this);

        m_textEditText = (EditText) findViewById(R.id.textShowInfoActivityEditText);
        Log.d("devcpp", "strName -> " + strName);
        Log.d("devcpp", "strSName -> " + strSName);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.yesShowInfoActivityButton:
                String strText = m_textEditText.getText().toString();
                Intent dataIntent = new Intent();
                dataIntent.putExtra(EXTRA_KEY_STRING_TEXT,strText);
                setResult(RESULT_OK, dataIntent);
                break;
            case R.id.noShowInfoActivityButton:
                setResult(RESULT_CANCELED);
                break;
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        Log.d("devcpp", "onBackPressed");
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public void finish() {
        Log.d("devcpp", "finish");
        super.finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp","onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp","onResume");
    }
}
