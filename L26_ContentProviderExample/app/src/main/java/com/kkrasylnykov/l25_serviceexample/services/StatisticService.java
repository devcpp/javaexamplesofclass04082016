package com.kkrasylnykov.l25_serviceexample.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kkrasylnykov.l25_serviceexample.toolsAndConstants.AppSettings;

import java.util.Timer;
import java.util.TimerTask;


public class StatisticService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("devcppStService","StatisticService -> onStartCommand -> ");
        getData();
        return super.onStartCommand(intent, flags, startId);
    }

    private void getData(){
        AppSettings appSettings = new AppSettings(this);
        for (int i=0; i<7; i++){
            Log.d("devcppStService","[" + i + "] -> " + appSettings.getCountClick(i));
        }

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                getData();
            }
        }, 10000);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
