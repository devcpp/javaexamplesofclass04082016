package com.kkrasylnykov.l25_serviceexample.toolsAndConstants;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.kkrasylnykov.l25_serviceexample.providers.SharedPreferenceContentProvider;

public class AppSettings {
    private Context m_context = null;

    public AppSettings(Context context){
        m_context = context;
    }

    public int getCountClick(int nId){
        Uri getCountUri = SharedPreferenceContentProvider.COUNT_EXEC_URI;
        Cursor cursor = m_context.getContentResolver().query(getCountUri,null,Integer.toString(nId),null,null);
        int nCount = 0;
        if (cursor!=null){
            if (cursor.moveToFirst()){
                nCount = cursor.getInt(0);
            }
            cursor.close();
        }
        return nCount;
    }

    public int addCountClick(int nId){
        Uri addCountUri = SharedPreferenceContentProvider.COUNT_EXEC_URI;
        m_context.getContentResolver().update(addCountUri, null, Integer.toString(nId), null);
        return getCountClick(nId);
    }
}
