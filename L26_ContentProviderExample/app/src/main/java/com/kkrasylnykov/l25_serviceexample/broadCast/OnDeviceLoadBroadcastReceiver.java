package com.kkrasylnykov.l25_serviceexample.broadCast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.StaticLayout;
import android.util.Log;

import com.kkrasylnykov.l25_serviceexample.services.StatisticService;

public class OnDeviceLoadBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("devcppStService","OnDeviceLoadBroadcastReceiver -> onReceive -> ");
        Intent intentService = new Intent(context, StatisticService.class);
        context.startService(intentService);
    }
}
