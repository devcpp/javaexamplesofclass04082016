package com.kkrasylnykov.l25_serviceexample.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kkrasylnykov.l25_serviceexample.R;
import com.kkrasylnykov.l25_serviceexample.activities.ViewerActivity;
import com.kkrasylnykov.l25_serviceexample.model.ImageInfo;
import com.kkrasylnykov.l25_serviceexample.services.DownloadImageService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class ImageFragment extends Fragment {

    private ImageView m_ImageView = null;
    private ImageInfo m_ImageInfo = null;
    private Bitmap m_Bitmap = null;
    private BroadcastReceiver m_BroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent!=null){
                Bundle bundle = intent.getExtras();
                int nType = -1;
                if (bundle!=null){
                    nType = bundle.getInt(DownloadImageService.KEY_ACTION_TYPE, DownloadImageService.ACTION_TYPE_ERROR);
                }

                if (nType==DownloadImageService.ACTION_TYPE_FINISH){
                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()+"/"+m_ImageInfo.getName()+".jpg");
                    loadImage(file);
                }
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        m_ImageView = (ImageView) view.findViewById(R.id.ImageView);
        return view;
    }

    public void setImagePath(String strImagePath){
        m_ImageInfo = new ImageInfo(strImagePath);
    }

    @Override
    public void onResume() {
        super.onResume();
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath()+"/"+m_ImageInfo.getName()+".jpg");
        if (file.exists()){
            loadImage(file);
        } else {
            Log.d("devcpp", "startService");
            Intent serviceIntent = new Intent(getActivity(), DownloadImageService.class);
            serviceIntent.putExtra(DownloadImageService.KEY_URL, m_ImageInfo.getUrl());
            serviceIntent.putExtra(DownloadImageService.KEY_FILE_PATH, file.getAbsolutePath());
            getActivity().startService(serviceIntent);

            getActivity().registerReceiver(m_BroadcastReceiver,new IntentFilter(DownloadImageService.ACTION_DOWNLOAD+file.getAbsolutePath()));
        }
    }

    private void loadImage(File file){
        if (m_Bitmap==null){
            if(file.exists()){

                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int nDispleyWidth = size.x;

                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inJustDecodeBounds = true;

                BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);

                int nFileWidth = bmOptions.outWidth;
                Log.d("devcpp","nFileWidth -> " + nFileWidth);
                Log.d("devcpp","nDispleyWidth -> " + nDispleyWidth);
                int nScale = (nFileWidth/nDispleyWidth);


                bmOptions = new BitmapFactory.Options();
                bmOptions.inSampleSize =nScale;
                m_Bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(),bmOptions);
                if (m_Bitmap!=null) {
                    Log.d("devcpp","m_Bitmap -> " + m_Bitmap.getWidth());
                }

            }
        }
        m_ImageView.setImageBitmap(m_Bitmap);
        m_ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewerActivity)getActivity()).nextPage();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        try{
            getActivity().unregisterReceiver(m_BroadcastReceiver);
        } catch (IllegalArgumentException e){
            e.printStackTrace();
        }

    }
}
