package com.kkrasylnykov.l25_serviceexample.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kkrasylnykov.l25_serviceexample.model.ImageInfo;

import java.util.ArrayList;

public class ImageViewAdapter extends BaseAdapter {

    private ArrayList<ImageInfo> m_arrData = null;

    public ImageViewAdapter(ArrayList<ImageInfo> m_arrData) {
        this.m_arrData = m_arrData;
    }

    @Override
    public int getCount() {
        return m_arrData.size();
    }

    @Override
    public Object getItem(int position) {
        return m_arrData.get(position).getName();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            convertView = new TextView(parent.getContext());
            convertView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));
        }
        ((TextView)convertView).setText((String)getItem(position));
        return convertView;
    }
}
