package com.kkrasylnykov.l25_serviceexample.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

public class SharedPreferenceContentProvider extends ContentProvider {

    private static final String KEY_IMAGE_COUNT_CLICK = "KEY_IMAGE_COUNT_CLICK";

    private static final String AUTHORITY = "com.kkrasylnykov.l25_serviceexample.providers.SharedPreferenceContentProvider";

    public static final String COUNT_EXEC     = "count";

    public static final Uri COUNT_EXEC_URI    = Uri.parse("content://" + AUTHORITY + "/" + COUNT_EXEC);

    private static final int URI_COUNT        = 1;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, COUNT_EXEC, URI_COUNT);
    }
    private SharedPreferences m_SharedPreferences = null;


    @Override
    public boolean onCreate() {
        m_SharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        MatrixCursor resultCursor = null;
        switch (uriMatcher.match(uri)){
            case URI_COUNT:
                int nCount = m_SharedPreferences.getInt(KEY_IMAGE_COUNT_CLICK+selection, 0);
                resultCursor = new MatrixCursor(new String[]{"DATA"});
                resultCursor.addRow(new Object[]{nCount});
                break;
        }
        return resultCursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int nResult = 0;
        switch (uriMatcher.match(uri)){
            case URI_COUNT:
                int nClickCount = m_SharedPreferences.getInt(KEY_IMAGE_COUNT_CLICK+selection,0) + 1;
                SharedPreferences.Editor editor = m_SharedPreferences.edit();
                editor.putInt(KEY_IMAGE_COUNT_CLICK+selection, nClickCount);
                editor.commit();
                nResult = 1;
                break;
        }
        return nResult;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }
}
