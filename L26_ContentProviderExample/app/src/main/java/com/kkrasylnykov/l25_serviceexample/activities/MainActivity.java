package com.kkrasylnykov.l25_serviceexample.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kkrasylnykov.l25_serviceexample.adapters.ImageViewAdapter;
import com.kkrasylnykov.l25_serviceexample.R;
import com.kkrasylnykov.l25_serviceexample.model.ImageInfo;
import com.kkrasylnykov.l25_serviceexample.toolsAndConstants.AppSettings;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView) findViewById(R.id.listView);
        ImageViewAdapter adapter = new ImageViewAdapter(getImagesInfo());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

    }

    private ArrayList<ImageInfo> getImagesInfo(){
        ArrayList<ImageInfo> arrData = new ArrayList<>();
        arrData.add(new ImageInfo("image_01", "http://4k-wallpaper.net/images/london-eye_303.jpeg"));
        arrData.add(new ImageInfo("image_02", "http://orig09.deviantart.net/8c03/f/2014/065/d/1/ergo_proxy___re_l_mayer___4k_ultra_hd_wallpaper_by_beethy-d796jhs.jpg"));
        arrData.add(new ImageInfo("image_03", "http://orig15.deviantart.net/e25b/f/2013/338/2/2/longhorn_evolution_on_4k_preview__by_fiazi-d6wn4sb.png"));
        arrData.add(new ImageInfo("image_04", "http://i.rtings.com/images/reviews/m-series-2015/m-series-2015-upscaling-4k-large.jpg"));
        arrData.add(new ImageInfo("image_05", "http://axeetech.com/wp-content/uploads/2015/08/Windows_10_4k_Wallpapers-1.jpeg"));
        arrData.add(new ImageInfo("image_06", "http://www.ultrahdwallpapers.net/images/6.jpg"));
        arrData.add(new ImageInfo("image_07", "http://4k-wallpaper.net/images/lake-louise-canada_113.jpeg"));
        return arrData;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, ViewerActivity.class);
        intent.putExtra(ViewerActivity.KEY_POSITION,position);
        ArrayList<ImageInfo> arrData = getImagesInfo();
        String[] arrStrData = new String[arrData.size()];
        for (int i=0;i<arrData.size();i++){
            arrStrData[i] = arrData.get(i).toString();
        }
        intent.putExtra(ViewerActivity.KEY_ARRAY_DATA,arrStrData);
        startActivity(intent);
        AppSettings appSettings = new AppSettings(this);
        Log.d("devcppApp", "appSettings.addCountClick -> " + appSettings.addCountClick(position));
    }
}
