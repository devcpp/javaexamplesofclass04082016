package l5_oopexample;

import java.util.Scanner;

public class L5_OOPExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        Quadrangle quadrangle = null;
        quadrangle = new Quadrangle();
        
        do{
            System.out.println("Menu: ");
            System.out.println("0. New quadrangle.");
            System.out.println("1. Input first side.");
            System.out.println("2. Input second side.");
            System.out.println("3. Input third side.");
            System.out.println("4. Input fourth side.");
            System.out.println("5. Get perimetr.");
            System.out.println("6. Is equilateral.");
            System.out.println("7. Exit.");
            int nTypeValue = scanner.nextInt();
            
            if (nTypeValue==7){
                break;
            }
            switch(nTypeValue){
                case 0:
                    quadrangle = new Quadrangle();
                    break;
                case 1:
                    float fSide1 = 0;
                    do{
                        System.out.print("Enter first side: ");
                        fSide1 = scanner.nextFloat();
                    } while(!quadrangle.setSide1(fSide1));                    
                    break;
                case 2:
                    float fSide2 = 0;
                    do{
                        System.out.print("Enter second side: ");
                        fSide2 = scanner.nextFloat();
                    } while(!quadrangle.setSide2(fSide2));                    
                    break;
                case 3:
                    float fSide3 = 0;
                    do{
                        System.out.print("Enter third side: ");
                        fSide3 = scanner.nextFloat();
                    } while(!quadrangle.setSide3(fSide3));                    
                    break;
                case 4:
                    float fSide4 = 0;
                    do{
                        System.out.print("Enter fourth side: ");
                        fSide4 = scanner.nextFloat();
                    } while(!quadrangle.setSide4(fSide4));                    
                    break;
                case 5:
                    if (quadrangle.isCorrectInput()){
                        System.out.println("Perimetr: " + quadrangle.getPerimetr());
                    } else {
                        System.err.println("Incorrect data!!!!");
                    }
                    break;
                case 6:
                    if (quadrangle.isCorrectInput()){
                        System.out.println("Is equilateral: " + quadrangle.isEquilateral());
                    } else {
                        System.err.println("Incorrect data!!!!");
                    }
                    break;
                default:
                    System.err.println("Incorrect input!!!!");
                    break;
            }
        } while(true);
        
        
    }
    
}
