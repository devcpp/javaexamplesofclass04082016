package l5_oopexample;

public class Quadrangle {
    
    private float m_fSide1 = 0f;
    private float m_fSide2 = 0f;
    private float m_fSide3 = 0f;
    private float m_fSide4 = 0f;
    
    public Quadrangle(){
        m_fSide1 = 0f;
        m_fSide2 = 0f;
        m_fSide3 = 0f;
        m_fSide4 = 0f;
    }
    
    public Quadrangle(float fVal){
        m_fSide1 = fVal;
        m_fSide2 = fVal;
        m_fSide3 = fVal;
        m_fSide4 = fVal;
    }
    
    public Quadrangle(float fVal1, float fVal2){
        this.m_fSide1 = fVal1;
        m_fSide2 = fVal2;
        m_fSide3 = fVal1;
        m_fSide4 = fVal2;
    }
    
    public float getPerimetr(){
        float fResurs = m_fSide1+m_fSide2+m_fSide3+m_fSide4;
        return fResurs;
    }
    
    public boolean isEquilateral(){
        boolean bResult = (m_fSide1==m_fSide3)&&(m_fSide2==m_fSide4);
        return bResult;
    }
    
    public boolean isCorrectInput(){
        boolean bResult = (m_fSide1>0)&&(m_fSide2>0)&&(m_fSide3>0)&&(m_fSide4>0);
        return bResult;
    }
    
    public boolean setSide1(float fVal){
        boolean bResult = false;
        if (fVal>0){
            m_fSide1 = fVal;
            bResult = true;
        }
        return bResult;
    }
    
    public boolean setSide2(float fVal){
        boolean bResult = false;
        if (fVal>0){
            m_fSide2 = fVal;
            bResult = true;
        }
        return bResult;
    }
    
    public boolean setSide3(float fVal){
        boolean bResult = false;
        if (fVal>0){
            m_fSide3 = fVal;
            bResult = true;
        }
        return bResult;
    }
    
    public boolean setSide4(float fVal){
        boolean bResult = false;
        if (fVal>0){
            m_fSide4 = fVal;
            bResult = true;
        }
        return bResult;
    }
    
    public float getSide1(){
        return m_fSide1;
    }
    
    public float getSide2(){
        return m_fSide2;
    }
    
    public float getSide3(){
        return m_fSide3;
    }
    
    public float getSide4(){
        return m_fSide4;
    }
    
}
