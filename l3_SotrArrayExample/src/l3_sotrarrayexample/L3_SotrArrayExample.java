package l3_sotrarrayexample;

import java.util.Scanner;

public class L3_SotrArrayExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Введите кол-во цифр:");
        int nCount = scanner.nextInt();
        
        int[] arrData = new int[nCount];
        
        for (int i=0; i<arrData.length; i++){
            arrData[i] = scanner.nextInt();
        }
        
        boolean bFlag = false;
        
        int nSize = arrData.length;
        
        do{
            nSize--;
            bFlag = false;
            for(int i=0; i<nSize; i++){
                if (arrData[i] > arrData[i+1]){
                    bFlag = true;
                    int nTmp = arrData[i];
                    arrData[i] = arrData[i+1];
                    arrData[i+1] = nTmp;
                }
            }
            
        } while(bFlag);
        
        for(int nVal:arrData){
            System.out.print(nVal+" ");
        }
        System.out.println();
        
        
        
    }
    
}
