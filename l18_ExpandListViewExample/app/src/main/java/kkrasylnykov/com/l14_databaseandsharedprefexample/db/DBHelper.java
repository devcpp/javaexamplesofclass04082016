package kkrasylnykov.com.l14_databaseandsharedprefexample.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

/**
 * Класс реализущий доступ к базе данных
 */
public class DBHelper extends SQLiteOpenHelper {

    /**
     * Конструктор класса. Для открытия БД используется Context.
     * @param context
     */
    public DBHelper(Context context) {
        /*В качестве параметров в конструктор родителя дополнительно передаем
        * имя базы данных (их может быть не одна) и ее (базы данных) версию.*/
        super(context, "db_example", null, 2);
    }

    /**
     * Функция вызываемая системой, в случае обращения к базе данных
     * которой не существует.
     * В данной функции происходит построение базы данных.
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DBConstants.TABLE_NAME_CONTACTS +
                " (" + DBConstants.TABLE_CONTACTS_FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                DBConstants.TABLE_CONTACTS_FIELD_NAME + " TEXT NOT NULL, " +
                DBConstants.TABLE_CONTACTS_FIELD_SNAME + " TEXT NOT NULL);");

        db.execSQL("CREATE TABLE " + DBConstants.TABLE_NAME_PHONES +
                " (" + DBConstants.TABLE_PHONES_FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                DBConstants.TABLE_PHONES_FIELD_USER_ID + " INTEGER NOT NULL, " +
                DBConstants.TABLE_PHONES_FIELD_PHONE + " TEXT NOT NULL);");
    }

    /**
     * Функция вызываемая системой, в случае обращения к базе данных
     * которая устарела.
     * В данной функции происходит обновление базы данных до текущей версии.
     * @param db существующая БД
     * @param oldVersion версия БД которая существует
     * @param currentVersion запращиваемая версия БД
     *
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int currentVersion) {
        if (oldVersion==1){
            Cursor cursor = db.query(DBConstants.TABLE_NAME_CONTACTS,null, null,null,null,null,null);
            ArrayList<HashMap<String,String>> arrData = new ArrayList<>();
            if (cursor!=null){
                if (cursor.moveToFirst()){
                    do {
                        HashMap<String,String> row = new HashMap<>();
                        row.put(DBConstants.TABLE_CONTACTS_FIELD_NAME,
                                cursor.getString(
                                        cursor.getColumnIndex(
                                                DBConstants.TABLE_CONTACTS_FIELD_NAME)));
                        row.put(DBConstants.TABLE_CONTACTS_FIELD_SNAME,
                                cursor.getString(
                                        cursor.getColumnIndex(
                                                DBConstants.TABLE_CONTACTS_FIELD_SNAME)));
                        row.put(DBConstants.TABLE_CONTACTS_OLD_FIELD_PHONE,
                                cursor.getString(
                                        cursor.getColumnIndex(
                                                DBConstants.TABLE_CONTACTS_OLD_FIELD_PHONE)));
                        arrData.add(row);
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }

            db.execSQL("DROP TABLE " + DBConstants.TABLE_NAME_CONTACTS + ";");

            onCreate(db);

            for (HashMap<String,String> currentHash:arrData){
                ContentValues valuesUserInfo = new ContentValues();
                valuesUserInfo.put(DBConstants.TABLE_CONTACTS_FIELD_NAME,
                        currentHash.get(DBConstants.TABLE_CONTACTS_FIELD_NAME));
                valuesUserInfo.put(DBConstants.TABLE_CONTACTS_FIELD_SNAME,
                        currentHash.get(DBConstants.TABLE_CONTACTS_FIELD_SNAME));
                long nId = db.insert(DBConstants.TABLE_NAME_CONTACTS, null, valuesUserInfo);

                ContentValues valuesPhone = new ContentValues();
                valuesPhone.put(DBConstants.TABLE_PHONES_FIELD_USER_ID, nId);
                valuesPhone.put(DBConstants.TABLE_PHONES_FIELD_PHONE,
                        currentHash.get(DBConstants.TABLE_CONTACTS_OLD_FIELD_PHONE));
                db.insert(DBConstants.TABLE_NAME_PHONES, null, valuesPhone);
            }
        }
    }
}
