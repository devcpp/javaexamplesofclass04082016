package kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.db_wrappers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.model.Phone;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class PhoneDBWrapper extends BaseDBWrapper {
    public PhoneDBWrapper(Context context) {
        super(context, DBConstants.TABLE_NAME_PHONES);
    }

    public ArrayList<Phone> getAll(){
        ArrayList<Phone> arrData = new ArrayList<>();
        SQLiteDatabase db = getReadable();
        Cursor cursor = db.query(getTableName(), null, null, null, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        Phone phone = new Phone(cursor);
                        arrData.add(phone);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrData;
    }

    public Phone getItemById(long nId){
        Phone phone = null;
        SQLiteDatabase db = getReadable();

        String strSelection = DBConstants.TABLE_PHONES_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(nId)};

        Cursor cursor = db.query(getTableName(), null,
                strSelection, arrSelectionArgs, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    phone = new Phone(cursor);
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return phone;
    }

    public ArrayList<Phone> getItemByUserId(long nUserId){
        ArrayList<Phone> arrData = new ArrayList<>();
        SQLiteDatabase db = getReadable();

        String strSelection = DBConstants.TABLE_PHONES_FIELD_USER_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(nUserId)};

        Cursor cursor = db.query(getTableName(), null, strSelection, arrSelectionArgs, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        Phone phone = new Phone(cursor);
                        arrData.add(phone);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrData;
    }

    public void insertItem(Phone item){
        SQLiteDatabase db = getWritable();
        ContentValues values = item.getContentValues();
        db.insert(getTableName(),null,values);
        db.close();
    }

    public void updateItem(Phone item){
        SQLiteDatabase db = getWritable();
        ContentValues values = item.getContentValues();
        String strSelection = DBConstants.TABLE_PHONES_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};
        db.update(getTableName(),values,strSelection, arrSelectionArgs);
        db.close();
    }

    public void deleteItemByUserId(long nUserId){
        SQLiteDatabase db = getWritable();
        String strSelection = DBConstants.TABLE_PHONES_FIELD_USER_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(nUserId)};
        db.delete(getTableName(),strSelection,arrSelectionArgs);
        db.close();
    }

    public void deleteItem(Phone item){
        SQLiteDatabase db = getWritable();
        String strSelection = DBConstants.TABLE_PHONES_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};
        db.delete(getTableName(),strSelection,arrSelectionArgs);
        db.close();
    }

    public void deleteAll(){
        SQLiteDatabase db = getWritable();
        db.delete(getTableName(),null,null);
        db.close();
    }
}
