package kkrasylnykov.com.l12_fragmentexample.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import kkrasylnykov.com.l12_fragmentexample.R;
import kkrasylnykov.com.l12_fragmentexample.activities.MainActivity;

public class FirstFragment extends Fragment implements View.OnClickListener {

    private EditText m_EditText = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("devL12", "FirstFragment -> onCreate -> ");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("devL12", "FirstFragment -> onCreateView -> ");
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        m_EditText = (EditText) view.findViewById(R.id.EditTextFirstFragment);
        View btn = view.findViewById(R.id.ButtonFirstFragment);
        btn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        ((MainActivity)getActivity()).setText(m_EditText.getText().toString());
    }

    @Override
    public void onStart() {
        Log.d("devL12", "FirstFragment -> onStart -> ");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d("devL12", "FirstFragment -> onResume -> ");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d("devL12", "FirstFragment -> onPause -> ");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d("devL12", "FirstFragment -> onStop -> ");
        super.onStop();
    }
}
