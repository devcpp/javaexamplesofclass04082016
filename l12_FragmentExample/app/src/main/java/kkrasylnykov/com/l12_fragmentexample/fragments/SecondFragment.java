package kkrasylnykov.com.l12_fragmentexample.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kkrasylnykov.com.l12_fragmentexample.R;

public class SecondFragment extends Fragment {

    private TextView m_TextView = null;
    private String m_strData = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("devL12", "SecondFragment -> onCreate -> ");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("devL12", "SecondFragment -> onCreateView -> ");
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        m_TextView = (TextView) view.findViewById(R.id.TextViewSecondFragment);
        m_TextView.setText(m_strData);
        return view;
    }

    public void setTextForTextView(String strText){
        m_strData = strText;
    }

    @Override
    public void onStart() {
        Log.d("devL12", "SecondFragment -> onStart -> " );
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d("devL12", "SecondFragment -> onResume -> ");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d("devL12", "SecondFragment -> onPause -> ");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d("devL12", "SecondFragment -> onStop -> ");
        super.onStop();
    }
}
