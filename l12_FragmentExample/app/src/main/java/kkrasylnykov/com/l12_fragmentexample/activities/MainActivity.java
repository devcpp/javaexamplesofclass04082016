package kkrasylnykov.com.l12_fragmentexample.activities;

import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import kkrasylnykov.com.l12_fragmentexample.R;
import kkrasylnykov.com.l12_fragmentexample.fragments.FirstFragment;
import kkrasylnykov.com.l12_fragmentexample.fragments.SecondFragment;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener {

    private FirstFragment m_FirstFragment = null;
    private SecondFragment m_SecondFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View btnAddFirstFragment = findViewById(R.id.AddFirstFragment);
        btnAddFirstFragment.setOnClickListener(this);
        View btnAddSecondFragment = findViewById(R.id.AddSecondFragment);
        btnAddSecondFragment.setOnClickListener(this);
        View btnRemoveFirstFragment = findViewById(R.id.RemoveFirstFragment);
        btnRemoveFirstFragment.setOnClickListener(this);
        View btnRemoveSecondFragment = findViewById(R.id.RemoveSecondFragment);
        btnRemoveSecondFragment.setOnClickListener(this);

        m_FirstFragment = new FirstFragment();
        m_SecondFragment = new SecondFragment();

    }

    public void setText(String strText){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayoutMainActivity,m_SecondFragment);
        transaction.commit();
        m_SecondFragment.setTextForTextView(strText);
    }

    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        switch (view.getId()){
            case R.id.AddFirstFragment:
                transaction.replace(R.id.frameLayoutMainActivity,m_FirstFragment);
                break;
            case R.id.AddSecondFragment:
                transaction.add(R.id.frameLayoutMainActivity,m_SecondFragment);
                break;
            case R.id.RemoveFirstFragment:
                transaction.remove(m_FirstFragment);
                break;
            case R.id.RemoveSecondFragment:
                transaction.remove(m_SecondFragment);
                break;
        }
        transaction.commit();

    }
}
