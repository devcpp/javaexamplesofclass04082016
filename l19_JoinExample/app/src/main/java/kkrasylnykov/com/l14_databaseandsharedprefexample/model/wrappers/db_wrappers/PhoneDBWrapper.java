package kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.db_wrappers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.model.Phone;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class PhoneDBWrapper extends BaseDBWrapper<Phone> {
    public PhoneDBWrapper(Context context) {
        super(context, DBConstants.TABLE_NAME_PHONES);
    }

    @Override
    protected Phone getInstance(Cursor cursor) {
        return new Phone(cursor);
    }

    public ArrayList<Phone> getItemByUserId(long nUserId){
        ArrayList<Phone> arrData = new ArrayList<>();
        SQLiteDatabase db = getReadable();

        String strSelection = DBConstants.TABLE_PHONES_FIELD_USER_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(nUserId)};

        Cursor cursor = db.query(getTableName(), null, strSelection, arrSelectionArgs, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        Phone phone = new Phone(cursor);
                        arrData.add(phone);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrData;
    }

    public void deleteItemById(long nId){
        SQLiteDatabase db = getWritable();
        String strSelection = DBConstants.TABLE_PHONES_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(nId)};
        db.delete(getTableName(),strSelection,arrSelectionArgs);
        db.close();
    }

    public void deleteItemByUserId(long nUserId){
        SQLiteDatabase db = getWritable();
        String strSelection = DBConstants.TABLE_PHONES_FIELD_USER_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(nUserId)};
        db.delete(getTableName(),strSelection,arrSelectionArgs);
        db.close();
    }
}
