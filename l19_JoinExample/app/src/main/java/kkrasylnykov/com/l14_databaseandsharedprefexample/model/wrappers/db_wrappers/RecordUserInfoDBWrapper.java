package kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.db_wrappers;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.model.Phone;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class RecordUserInfoDBWrapper extends BaseDBWrapper<RecordUserInfo> {

    public RecordUserInfoDBWrapper(Context context) {
        super(context, DBConstants.TABLE_NAME_CONTACTS);
    }

    @Override
    protected RecordUserInfo getInstance(Cursor cursor) {
        return new RecordUserInfo(cursor);
    }

    @Override
    public ArrayList<RecordUserInfo> getAll() {
        ArrayList<RecordUserInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadable();
        Cursor cursor = db.rawQuery("SELECT c._id, c._name, c._sname, p._id,  p._phone FROM " + DBConstants.TABLE_NAME_CONTACTS +
                " c JOIN " + DBConstants.TABLE_NAME_PHONES +
                " p ON c."+DBConstants.TABLE_CONTACTS_FIELD_ID +
                "=p."+DBConstants.TABLE_PHONES_FIELD_USER_ID,null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    RecordUserInfo oldUserInfo = null;
                    do{
                        long nIdUser = cursor.getLong(0);
                        long nPhoneId = -1;
                        String strPhone = "";
                        if (oldUserInfo!=null && nIdUser==oldUserInfo.getId()){
                            nPhoneId = cursor.getLong(3);
                            strPhone = cursor.getString(4);
                        } else {
                            if (oldUserInfo!=null){
                                arrResult.add(oldUserInfo);
                            }
                            String strUserName = cursor.getString(1);
                            String strUserSName = cursor.getString(2);
                            nPhoneId = cursor.getLong(3);
                            strPhone = cursor.getString(4);
                            oldUserInfo = new RecordUserInfo(nIdUser, strUserName, strUserSName,
                                    new ArrayList<Phone>());
                        }
                        oldUserInfo.getPhones().add(new Phone(nPhoneId, nIdUser, strPhone));
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return arrResult;
    }
}
