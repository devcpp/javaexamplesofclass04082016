package kkrasylnykov.com.l14_databaseandsharedprefexample.model.engines;

import android.content.Context;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.model.Phone;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.db_wrappers.PhoneDBWrapper;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.db_wrappers.RecordUserInfoDBWrapper;

public class PhoneEngine {

    private Context m_context = null;

    public PhoneEngine(Context context){
        m_context = context;
    }

    public ArrayList<Phone> getAll(){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(m_context);
        return wrapper.getAll();
    }

    public Phone getItemById(long nId){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(m_context);
        return wrapper.getItemById(nId);
    }

    public ArrayList<Phone> getItemByUserId(long nUserId){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(m_context);
        return wrapper.getItemByUserId(nUserId);
    }

    public void insertItem(Phone item){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(m_context);
        long nId = wrapper.insertItem(item);
    }

    public void updateItem(Phone item){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(m_context);
        wrapper.updateItem(item);
    }

    public void deleteItem(Phone item){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(m_context);
        wrapper.deleteItem(item);
    }

    public void deleteItemById(long nId){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(m_context);
        wrapper.deleteItemById(nId);
    }

    public void deleteItemByUserId(long nUserId){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(m_context);
        wrapper.deleteItemByUserId(nUserId);
    }

    public void deleteAll(){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(m_context);
        wrapper.deleteAll();
    }
}
