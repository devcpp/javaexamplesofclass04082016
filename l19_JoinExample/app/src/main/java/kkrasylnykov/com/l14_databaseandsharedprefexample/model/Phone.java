package kkrasylnykov.com.l14_databaseandsharedprefexample.model;

import android.content.ContentValues;
import android.database.Cursor;

import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class Phone extends Entity {

    private long m_nUserId = -1;
    private String m_strPhone = "";

    public Phone(long nId, long nUserId, String m_strPhone) {
        this(m_strPhone);
        this.m_nUserId = nUserId;
        setId(nId);
    }

    public Phone(String m_strPhone) {
        setId(-1);
        this.m_nUserId = -1;
        this.m_strPhone = m_strPhone;
    }

    public Phone(Cursor cursor){
        setId(cursor.getLong(cursor.getColumnIndex(DBConstants.TABLE_PHONES_FIELD_ID)));
        this.m_nUserId = cursor.getLong(cursor.getColumnIndex(DBConstants.TABLE_PHONES_FIELD_USER_ID));
        this.m_strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_PHONES_FIELD_PHONE));
    }

    public long getUserId() {
        return m_nUserId;
    }

    public void setUserId(long m_nUserId) {
        this.m_nUserId = m_nUserId;
    }

    public String getPhone() {
        return m_strPhone;
    }

    public void setPhone(String m_strPhone) {
        this.m_strPhone = m_strPhone;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(DBConstants.TABLE_PHONES_FIELD_USER_ID, getUserId());
        values.put(DBConstants.TABLE_PHONES_FIELD_PHONE, getPhone());
        return values;
    }
}
