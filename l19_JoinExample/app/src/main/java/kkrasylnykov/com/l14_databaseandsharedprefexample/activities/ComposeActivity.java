package kkrasylnykov.com.l14_databaseandsharedprefexample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.R;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.Phone;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.engines.PhoneEngine;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.engines.RecordUserInfoEngine;


public class ComposeActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_KEY_ID = "EXTRA_KEY_ID";

    /*private long m_nId = -1;*/
    private RecordUserInfo m_userInfo = null;

    private EditText m_nameEditText;
    private EditText m_snameEditText;
    private EditText[] m_phonesEditTex = new EditText[5];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose);

        Button btnAdd = (Button) findViewById(R.id.addButtonComposeActivity);
        btnAdd.setOnClickListener(this);

        Button btnRemove = (Button) findViewById(R.id.removeButtonComposeActivity);
        btnRemove.setOnClickListener(this);

        m_nameEditText = (EditText) findViewById(R.id.nameEditTextComposeActivity);
        m_snameEditText = (EditText) findViewById(R.id.snameEditTextComposeActivity);
        m_phonesEditTex[0] = (EditText) findViewById(R.id.phone1EditTextComposeActivity);
        m_phonesEditTex[1] = (EditText) findViewById(R.id.phone2EditTextComposeActivity);
        m_phonesEditTex[2] = (EditText) findViewById(R.id.phone3EditTextComposeActivity);
        m_phonesEditTex[3] = (EditText) findViewById(R.id.phone4EditTextComposeActivity);
        m_phonesEditTex[4] = (EditText) findViewById(R.id.phone5EditTextComposeActivity);


        Intent intent = getIntent();
        long nId = -1;
        if(intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                nId = bundle.getLong(EXTRA_KEY_ID,-1);
            }
        }

        if (nId!=-1){
            btnAdd.setText("Update");

            btnRemove.setVisibility(View.VISIBLE);

            RecordUserInfoEngine engine = new RecordUserInfoEngine(this);
            m_userInfo = engine.getItemById(nId);
            m_nameEditText.setText(m_userInfo.getName());
            m_snameEditText.setText(m_userInfo.getSName());
            ArrayList<Phone> phones = m_userInfo.getPhones();
            for (int i=0;i<phones.size();i++){
                if (i>=m_phonesEditTex.length){
                    break;
                }
                m_phonesEditTex[i].setText(phones.get(i).getPhone());
                m_phonesEditTex[i].setTag(phones.get(i).getId());
            }
        }
    }

    @Override
    public void onClick(View v) {
        RecordUserInfoEngine engine = new RecordUserInfoEngine(this);
        switch (v.getId()){
            case R.id.addButtonComposeActivity:
                if (m_userInfo==null){
                    ArrayList<Phone> arrPhones = new ArrayList<>();
                    for (int i=0;i<m_phonesEditTex.length; i++){
                        String strPhone = m_phonesEditTex[i].getText().toString();
                        if (!strPhone.isEmpty()){
                            arrPhones.add(new Phone(strPhone));
                        }
                    }
                    m_userInfo = new RecordUserInfo(m_nameEditText.getText().toString(),
                            m_snameEditText.getText().toString(),
                            arrPhones);
                } else {
                    ArrayList<Phone> arrPhones = new ArrayList<>();
                    for (int i=0;i<m_phonesEditTex.length; i++){
                        String strPhone = m_phonesEditTex[i].getText().toString();
                        if (!strPhone.isEmpty()){
                            long nIdPhone = -1;
                            if (m_phonesEditTex[i].getTag()!=null){
                                nIdPhone = (long) m_phonesEditTex[i].getTag();
                            }
                            arrPhones.add(new Phone(nIdPhone, m_userInfo.getId(), strPhone));
                        } else if (m_phonesEditTex[i].getTag()!=null) {
                            long nIdPhone = (long) m_phonesEditTex[i].getTag();
                            PhoneEngine phoneEngine = new PhoneEngine(this);
                            phoneEngine.deleteItemById(nIdPhone);
                        }
                    }
                    m_userInfo.setName(m_nameEditText.getText().toString());
                    m_userInfo.setSName(m_snameEditText.getText().toString());
                    m_userInfo.setPhones(arrPhones);
                }

                if(m_userInfo.getId()!=-1){
                    engine.updateItem(m_userInfo);
                } else {
                    engine.insertItem(m_userInfo);
                }
                break;

            case R.id.removeButtonComposeActivity:
                engine.deleteItem(m_userInfo);
                break;
        }
        finish();
    }
}
