package l3_arrayexample1;

import java.util.Scanner;

public class L3_ArrayExample1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Введите кол-во сторон:");
        int nCount = scanner.nextInt();
        
        float[] arrSides = new float[nCount];
        
        for (int i=0; i<arrSides.length; i++){
            float fVal = 0;
            do{
                System.out.print("Введите размер стороны: ");
                fVal = scanner.nextFloat();
                if (fVal>0){
                    break;
                } else {
                    System.err.println("Значение должно быть больше 0!");
                }
            } while(true);
            arrSides[i] = fVal;
        }
        
        float fSum = 0;
        
        for(float fValForSum:arrSides){
            fSum+=fValForSum;
        }
        
        //System.out.println("Сумма: " + fSum);
        
        System.out.print("Сумма: ");
        System.out.println(fSum);
    }
    
}
