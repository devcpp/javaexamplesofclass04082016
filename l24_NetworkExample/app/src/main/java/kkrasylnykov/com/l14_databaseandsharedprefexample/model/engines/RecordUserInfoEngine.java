package kkrasylnykov.com.l14_databaseandsharedprefexample.model.engines;

import android.content.Context;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.model.Phone;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.db_wrappers.RecordUserInfoDBWrapper;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.network_wrappers.UserInfoNetworkWrapper;

public class RecordUserInfoEngine {

    private Context m_context = null;

    public RecordUserInfoEngine(Context context){
        m_context = context;
    }

    public ArrayList<RecordUserInfo> loadUserInfoFromServer(){
        UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper(m_context);
        ArrayList<RecordUserInfo> arrData = networkWrapper.getAll();
        for (RecordUserInfo userInfo:arrData){
            insertItem(userInfo);
        }
        return arrData;
    }

    public ArrayList<RecordUserInfo> getAll(){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        ArrayList<RecordUserInfo> arrData = wrapper.getAll();
        PhoneEngine phoneEngine = new PhoneEngine(m_context);

        for(int i=0; i<arrData.size();i++){
            RecordUserInfo info = arrData.get(i);
            ArrayList<Phone> arrPhone = phoneEngine.getItemByUserId(info.getId());
            info.setPhones(arrPhone);
        }
        return arrData;
    }

    public RecordUserInfo getItemById(long nId){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        RecordUserInfo info = wrapper.getItemById(nId);
        PhoneEngine phoneEngine = new PhoneEngine(m_context);
        info.setPhones(phoneEngine.getItemByUserId(info.getId()));
        return info;
    }

    public void insertItem(RecordUserInfo item){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        if (item.getServerId()==-1){
            UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper(m_context);
            item = networkWrapper.insertItem(item);
        }
        long nUserId = wrapper.insertItem(item);
        ArrayList<Phone> arrPhones = item.getPhones();
        if (arrPhones!=null){
            PhoneEngine phoneEngine = new PhoneEngine(m_context);
            for (Phone phone:arrPhones){
                phone.setUserId(nUserId);
                phoneEngine.insertItem(phone);
            }
        }
    }

    public void updateItem(RecordUserInfo item){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        wrapper.updateItem(item);
        ArrayList<Phone> arrPhones = item.getPhones();
        if (arrPhones!=null){
            PhoneEngine phoneEngine = new PhoneEngine(m_context);
            for (Phone phone:arrPhones){
                if (phone.getId()==-1){
                    phone.setUserId(item.getId());
                    phoneEngine.insertItem(phone);
                } else {
                    phoneEngine.updateItem(phone);
                }

            }
        }

        UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper(m_context);
        networkWrapper.updateItem(item);
    }

    public void deleteItem(RecordUserInfo item){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        wrapper.deleteItem(item);
        PhoneEngine phoneEngine = new PhoneEngine(m_context);
        phoneEngine.deleteItemByUserId(item.getId());
        UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper(m_context);
        networkWrapper.deleteItem(item);
    }

    public void deleteAll(){
        RecordUserInfoDBWrapper wrapper = new RecordUserInfoDBWrapper(m_context);
        ArrayList<RecordUserInfo> arrData = wrapper.getAll();
        wrapper.deleteAll();
        PhoneEngine phoneEngine = new PhoneEngine(m_context);
        phoneEngine.deleteAll();
        UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper(m_context);
        for (RecordUserInfo userInfo:arrData){
            networkWrapper.deleteItem(userInfo);
        }
    }
}
