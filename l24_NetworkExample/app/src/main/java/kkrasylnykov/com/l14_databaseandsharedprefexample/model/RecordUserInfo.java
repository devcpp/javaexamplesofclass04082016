package kkrasylnykov.com.l14_databaseandsharedprefexample.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class RecordUserInfo extends Entity {

    private long m_nServerId = -1;
    private String m_strName = "";
    private String m_strSName = "";
    private ArrayList<Phone> m_arrPhones = null;

    public RecordUserInfo(String m_strName, String m_strSName, ArrayList<Phone> m_arrPhones) {
        setId(-1);
        this.m_nServerId = -1;
        this.m_strName = m_strName;
        this.m_strSName = m_strSName;
        this.m_arrPhones = m_arrPhones;
    }

    public RecordUserInfo(Cursor cursor){
        setId(cursor.getLong(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_ID)));
        this.m_nServerId = cursor.getLong(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_SERVER_ID));
        this.m_strName = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_NAME));
        this.m_strSName = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_SNAME));
    }

    public RecordUserInfo(JSONObject jsonObject) throws JSONException {
        this.m_nServerId = jsonObject.getInt("id");
        this.m_strName = jsonObject.getString("name");
        this.m_strSName = jsonObject.getString("sname");
        this.m_arrPhones = new ArrayList<>();
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public ArrayList<Phone> getPhones() {
        return m_arrPhones;
    }

    public void setPhones(ArrayList<Phone> m_arrPhones) {
        this.m_arrPhones = m_arrPhones;
    }

    public long getServerId() {
        return m_nServerId;
    }

    public void setServerId(long nServerId) {
        m_nServerId = nServerId;
    }

    @Override
    public String toString() {
        String strPhones = "";
        if (m_arrPhones!=null){
            for(Phone phone:m_arrPhones){
                if (strPhones.isEmpty()){
                    strPhones+=phone.getPhone();
                } else {
                    strPhones+="/n"+phone.getPhone();
                }
            }
            if (!strPhones.isEmpty()){
                strPhones="\nPhones: "+strPhones;
            }
        }

        return "strName: " + m_strName +
                "\nstrSName: " + m_strSName + strPhones;

    }

    @Override
    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.TABLE_CONTACTS_FIELD_SERVER_ID, m_nServerId);
        values.put(DBConstants.TABLE_CONTACTS_FIELD_NAME, m_strName);
        values.put(DBConstants.TABLE_CONTACTS_FIELD_SNAME, m_strSName);

        return values;
    }

    public JSONObject getJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();

        if (getServerId()!=-1){
            jsonObject.put("id",getServerId());
        }
        jsonObject.put("name",getName());
        jsonObject.put("sname",getSName());
        JSONArray jsonArray = new JSONArray();
        for (Phone phone:getPhones()){
            jsonArray.put(phone.getPhone());
        }
        jsonObject.put("phones",jsonArray);
        return jsonObject;
    }
}
