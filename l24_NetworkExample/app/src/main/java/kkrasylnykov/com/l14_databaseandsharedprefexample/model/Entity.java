package kkrasylnykov.com.l14_databaseandsharedprefexample.model;


import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public abstract class Entity {

    private long m_nId = -1;

    public Entity(){
    }

    public Entity(Cursor cursor) {
    }

    public Entity(JSONObject jsonObject){

    }

    public long getId() {
        return m_nId;
    }

    protected void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public abstract ContentValues getContentValues();
}
