package kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.db_wrappers;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class RecordUserInfoDBWrapper extends BaseDBWrapper<RecordUserInfo> {

    public RecordUserInfoDBWrapper(Context context) {
        super(context, DBConstants.TABLE_NAME_CONTACTS);
    }


    @Override
    protected RecordUserInfo getInstance(Cursor cursor) {
        return new RecordUserInfo(cursor);
    }

}
