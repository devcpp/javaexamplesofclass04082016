package kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.network_wrappers;


import android.content.Context;
import android.widget.BaseAdapter;

public class BaseNetworkWrapper {

    private static final String BASE_URL = "http://xutpuk.pp.ua/";

    private Context m_Context = null;

    public BaseNetworkWrapper(Context context) {
        this.m_Context = context;
    }

    public Context getContext() {
        return m_Context;
    }

    public static String getBaseUrl() {
        return BASE_URL;
    }
}
