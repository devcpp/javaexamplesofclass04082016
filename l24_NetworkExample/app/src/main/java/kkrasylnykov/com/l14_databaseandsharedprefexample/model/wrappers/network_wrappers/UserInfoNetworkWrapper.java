package kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.network_wrappers;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;

public class UserInfoNetworkWrapper extends BaseNetworkWrapper {
    public UserInfoNetworkWrapper(Context context) {
        super(context);
    }

    public ArrayList<RecordUserInfo> getAll(){
        ArrayList<RecordUserInfo> arrResult = new ArrayList<>();
        try {
            URL url = new URL(getBaseUrl() + "api/users.json?_format=json");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("GET");
            connection.setRequestProperty( "Content-Type", "application/json");

            connection.connect();

            int nResponceCode = connection.getResponseCode();
            if (nResponceCode==200){
                InputStream is = connection.getInputStream();
                String strResponse = "";
                if (is!=null){
                    BufferedReader reader = null;
                    reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    if (reader!=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        while (true){
                            String tempStr = reader.readLine();
                            if(tempStr != null){
                                stringBuilder.append(tempStr);
                            } else {
                                strResponse = stringBuilder.toString();
                                break;
                            }
                        }
                    }
                }
                JSONArray arrData = new JSONArray(strResponse);
                for (int i=0; i<arrData.length(); i++){
                    JSONObject jsonObject = arrData.getJSONObject(i);
                    arrResult.add(new RecordUserInfo(jsonObject));
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //
        return arrResult;
    }

    public void deleteItem(RecordUserInfo item){
        try {
            URL url = new URL(getBaseUrl() + "api/users/" + item.getServerId() + ".json");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("DELETE");
            connection.setRequestProperty( "Content-Type", "application/json");

            connection.connect();

            int nResponceCode = connection.getResponseCode();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public RecordUserInfo insertItem(RecordUserInfo item){
        try {
            URL url = new URL(getBaseUrl() + "api/users.json");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("POST");
            connection.setRequestProperty( "Content-Type", "application/json");

            JSONObject jsonItem = item.getJSON();
            String strBody = jsonItem.toString();

            byte[] postData = strBody.getBytes(StandardCharsets.UTF_8);
            connection.getOutputStream().write(postData);

            connection.connect();

            int nResponceCode = connection.getResponseCode();
            if (nResponceCode==200){
                InputStream is = connection.getInputStream();
                String strResponse = "";
                if (is!=null){
                    BufferedReader reader = null;
                    reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    if (reader!=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        while (true){
                            String tempStr = reader.readLine();
                            if(tempStr != null){
                                stringBuilder.append(tempStr);
                            } else {
                                strResponse = stringBuilder.toString();
                                break;
                            }
                        }
                    }
                }
                JSONObject jsonObject = new JSONObject(strResponse);
                long nServerId = new RecordUserInfo(jsonObject).getServerId();
                item.setServerId(nServerId);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return item;
    }

    public void updateItem(RecordUserInfo item){
        try {
            URL url = new URL(getBaseUrl() + "api/users.json");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("PUT");
            connection.setRequestProperty( "Content-Type", "application/json");

            JSONObject jsonItem = item.getJSON();
            String strBody = jsonItem.toString();

            byte[] postData = strBody.getBytes(StandardCharsets.UTF_8);
            connection.getOutputStream().write(postData);

            connection.connect();

            int nResponceCode = connection.getResponseCode();
            if (nResponceCode==200){
//                InputStream is = connection.getInputStream();
//                String strResponse = "";
//                if (is!=null){
//                    BufferedReader reader = null;
//                    reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
//                    if (reader!=null){
//                        StringBuilder stringBuilder = new StringBuilder();
//                        while (true){
//                            String tempStr = reader.readLine();
//                            if(tempStr != null){
//                                stringBuilder.append(tempStr);
//                            } else {
//                                strResponse = stringBuilder.toString();
//                                break;
//                            }
//                        }
//                    }
//                }
//                JSONObject jsonObject = new JSONObject(strResponse);
//                long nServerId = new RecordUserInfo(jsonObject).getServerId();
//                item.setServerId(nServerId);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
