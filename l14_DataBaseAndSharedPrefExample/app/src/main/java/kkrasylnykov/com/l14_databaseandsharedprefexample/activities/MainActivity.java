package kkrasylnykov.com.l14_databaseandsharedprefexample.activities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import kkrasylnykov.com.l14_databaseandsharedprefexample.R;
import kkrasylnykov.com.l14_databaseandsharedprefexample.db.DBHelper;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.AppSettings;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Создаем класс настроек
        AppSettings appSettings = new AppSettings(this);
        //Проверяем - первый раз мы запускаем приложение или нет
        if (appSettings.getIsFirstStart()){
            //Если первый раз - устанавливаем флаг в false, что бы не допустить повторного вызова метода
            appSettings.setIsFirstStart(false);
            //Показываем сообщение на экран
            Toast.makeText(this, "This is first start app!", Toast.LENGTH_LONG).show();
            //Создаем класс-помощника работы с БД
            DBHelper dbHelper = new DBHelper(this);
            //Получаем базу данных для записи
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            //Создаем класс для передачи данных в БД (в формате ключь-значение).
            ContentValues values = new ContentValues();
            //Добавляем данные в объект
            //В качестве ключа используем имена столбцов.
            values.put(DBConstants.TABLE_CONTACTS_FIELD_NAME, "kos");
            values.put(DBConstants.TABLE_CONTACTS_FIELD_SNAME, "kras");
            values.put(DBConstants.TABLE_CONTACTS_FIELD_PHONE, "095");
            //Записываем их в БД
            db.insert(DBConstants.TABLE_NAME,null,values);

            values.clear();
            values.put(DBConstants.TABLE_CONTACTS_FIELD_NAME, "kos1");
            values.put(DBConstants.TABLE_CONTACTS_FIELD_SNAME, "kras2");
            values.put(DBConstants.TABLE_CONTACTS_FIELD_PHONE, "0953");
            db.insert(DBConstants.TABLE_NAME,null,values);

            values.clear();
            values.put(DBConstants.TABLE_CONTACTS_FIELD_NAME, "kos3");
            values.put(DBConstants.TABLE_CONTACTS_FIELD_SNAME, "kras4");
            values.put(DBConstants.TABLE_CONTACTS_FIELD_PHONE, "09535");
            db.insert(DBConstants.TABLE_NAME,null,values);

            //После окончания работы с БД - ОБЯЗАТЕЛЬНО ЗАКРЫВАЕМ БД!
            db.close();
            //После закрытия БД - ОБЯЗАТЕЛЬНО ЗАКРЫВАЕМ КЛАСС-ПОМОЩНИК!
            dbHelper.close();
        }


        DBHelper dbHelper = new DBHelper(this);
        //Получаем базу данных для чтения
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //Формируем запрос к БД
        Cursor cursor = db.query(DBConstants.TABLE_NAME, null, null, null, null, null, null);
        //Если Cursor не null
        if (cursor!=null){
            try{
                //Проверяем, может ли Cursor перейти на первый элемент
                if (cursor.moveToFirst()){
                    do{
                        //Считываем из курсора данные по каждому элементу
                        long id = cursor.getLong(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_ID));
                        String strName = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_NAME));
                        String strSName = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_SNAME));
                        String strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_PHONE));
                        Log.d("l14_devcpp", "id: " + id + "; strName: " + strName +
                                "; strSName: " + strSName + "; strPhone: " + strPhone);
                    } while (cursor.moveToNext()); //Выходим из цикла в случае если Cursor не может перейти к следующему элементу (достиг конца или потерял связь с данными)
                }
            } finally {
                //После окончания работы с Cursor - ОБЗАТЕЛЬНО ЕГО ЗАКРЫТЬ!
                cursor.close();
            }
        }
        db.close();
        dbHelper.close();

    }
}
