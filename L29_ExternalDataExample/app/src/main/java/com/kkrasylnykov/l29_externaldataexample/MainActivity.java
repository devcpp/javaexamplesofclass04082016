package com.kkrasylnykov.l29_externaldataexample;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final String AUTHORITY = "kkrasylnykov.com.l14_databaseandsharedprefexample.contentProviders.DataContentProvider";

    public static final String COTACTS     = "contacts";
    public static final String PHONES     = "phone";

    public static final Uri COTACTS_URI    = Uri.parse("content://" + AUTHORITY + "/" + COTACTS);
    public static final Uri PHONES_URI    = Uri.parse("content://" + AUTHORITY + "/" + PHONES);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ContentValues values = new ContentValues();
        values.put("_name", "Ivan");
        values.put("_sname", "Ivanov");
        values.put("_phone", "987654321");
        getContentResolver().insert(COTACTS_URI,values);

        Cursor cursor = getContentResolver().query(COTACTS_URI, null, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        long nId = cursor.getLong(0);
                        String strName = cursor.getString(1);
                        String strSName = cursor.getString(2);
                        String strPhone = cursor.getString(3);

                        Log.d("devcppdata","nId -> " + nId + "; strName -> " + strName
                                + "; strSName -> " + strSName +"; strPhone -> " + strPhone);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }

    }
}
