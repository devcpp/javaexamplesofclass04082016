
package l1_var;

import java.util.Scanner;

public class L1_Var {

    public static void main(String[] args) {
        int nVar1 = 10;
        int nVar2 = 15;
        
        float fVar3 = (float)nVar1/nVar2;
        
        String strResult = nVar1 + "" + nVar2;
        
        int nVar4 = 0;
        int nVar5 = 0;
        
        Scanner scan = new Scanner(System.in);
        
        nVar4 = scan.nextInt();
        nVar5 = scan.nextInt();
        
        int nVar6 = nVar4 + nVar5;
        
        System.out.println(fVar3);
        System.out.println(strResult);
        System.out.println(nVar6);
    }
    
}
