package com.kkrasylnykov.l9_eventexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView m_helloWorldTextView = null;
    private EditText m_inputEditText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_helloWorldTextView =
                (TextView) findViewById(R.id.helloWorldTextViewMainActivity);
        //helloWorldTextView.setText("Измененная строка!!!");
        m_helloWorldTextView.setText(R.string.string_for_main_screen);
        m_helloWorldTextView.setTextColor(getResources().getColor(R.color.colorForTextOnMainActivity));

        Button btnYes = (Button) findViewById(R.id.yesButtonMainActivity);
        btnYes.setOnClickListener(this);

        Button btnNo = (Button) findViewById(R.id.noButtonMainActivity);
        btnNo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (m_inputEditText==null){
            m_inputEditText = (EditText) findViewById(R.id.inputEditTextMainActivity);
        }

        switch (view.getId()){
            case R.id.yesButtonMainActivity:
                String strText = m_inputEditText.getText().toString();
                if (strText.isEmpty()){
                    return;
                }
                m_inputEditText.setText("");
                strText = "Вы ввели: " + strText;
                m_helloWorldTextView.setText(strText);
                m_helloWorldTextView.setVisibility(View.VISIBLE);
                break;
            case R.id.noButtonMainActivity:
                m_helloWorldTextView.setText("");
                m_helloWorldTextView.setVisibility(View.GONE);
                m_inputEditText.setText("");
                break;
        }
    }
}
