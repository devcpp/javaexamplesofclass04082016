package kkrasylnykov.com.l14_databaseandsharedprefexample.contentProviders;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

import kkrasylnykov.com.l14_databaseandsharedprefexample.db.DBHelper;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class DataContentProvider extends ContentProvider {
    private static final String AUTHORITY = "kkrasylnykov.com.l14_databaseandsharedprefexample.contentProviders.DataContentProvider";

    public static final String COTACTS     = "contacts";
    public static final String PHONES     = "phone";

    public static final Uri COTACTS_URI    = Uri.parse("content://" + AUTHORITY + "/" + COTACTS);
    public static final Uri PHONES_URI    = Uri.parse("content://" + AUTHORITY + "/" + PHONES);

    private static final int URI_COTACTS      = 1;
    private static final int URI_PHONES      = 2;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, COTACTS, URI_COTACTS);
        uriMatcher.addURI(AUTHORITY, PHONES, URI_PHONES);
    }

    DBHelper m_dbHelper;

    @Override
    public boolean onCreate() {
        m_dbHelper = new DBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = m_dbHelper.getReadableDatabase();
        return db.query(getType(uri),projection,selection,selectionArgs,null,null,sortOrder);
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        String strTableName = "";
        switch (uriMatcher.match(uri)) {
            case URI_COTACTS:
                strTableName = DBConstants.TABLE_NAME;
                break;
            case URI_PHONES:
                strTableName = "URI_PHONES";//DBConstants.TABLE_NAME;
                break;
        }
        return strTableName;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        db.insert(getType(uri),null,values);
        return uri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        return db.delete(getType(uri),selection,selectionArgs);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = m_dbHelper.getWritableDatabase();
        return db.update(getType(uri),values,selection,selectionArgs);
    }
}
