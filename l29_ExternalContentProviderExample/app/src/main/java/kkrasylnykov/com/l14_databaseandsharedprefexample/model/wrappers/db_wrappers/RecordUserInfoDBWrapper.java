package kkrasylnykov.com.l14_databaseandsharedprefexample.model.wrappers.db_wrappers;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kkrasylnykov.com.l14_databaseandsharedprefexample.contentProviders.DataContentProvider;
import kkrasylnykov.com.l14_databaseandsharedprefexample.model.RecordUserInfo;
import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class RecordUserInfoDBWrapper extends BaseDBWrapper {
    private Context m_context;

    public RecordUserInfoDBWrapper(Context context) {
        super(context, DBConstants.TABLE_NAME);
        m_context = context;
    }

    public ArrayList<RecordUserInfo> getAll(){
        ArrayList<RecordUserInfo> arrData = new ArrayList<>();
        //SQLiteDatabase db = getReadable();
        Cursor cursor = m_context.getContentResolver().query(DataContentProvider.COTACTS_URI, null, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    do{
                        RecordUserInfo userInfo = new RecordUserInfo(cursor);
                        arrData.add(userInfo);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
        return arrData;
    }

    public RecordUserInfo getItemById(long nId){
        RecordUserInfo userInfo = null;
        SQLiteDatabase db = getReadable();

        String strSelection = DBConstants.TABLE_CONTACTS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(nId)};

        Cursor cursor = db.query(getTableName(), null,
                strSelection, arrSelectionArgs, null, null, null);
        if (cursor!=null){
            try{
                if (cursor.moveToFirst()){
                    userInfo = new RecordUserInfo(cursor);
                }
            } finally {
                cursor.close();
            }
        }
        db.close();
        return userInfo;
    }

    public void insertItem(RecordUserInfo item){
        SQLiteDatabase db = getWritable();
        ContentValues values = item.getContentValues();
        db.insert(getTableName(),null,values);
        db.close();
    }

    public void updateItem(RecordUserInfo item){
        SQLiteDatabase db = getWritable();
        ContentValues values = item.getContentValues();
        String strSelection = DBConstants.TABLE_CONTACTS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};
        db.update(getTableName(),values,strSelection, arrSelectionArgs);
        db.close();
    }

    public void deleteItem(RecordUserInfo item){
        SQLiteDatabase db = getWritable();
        String strSelection = DBConstants.TABLE_CONTACTS_FIELD_ID + "=?";
        String[] arrSelectionArgs = {Long.toString(item.getId())};
        db.delete(getTableName(),strSelection,arrSelectionArgs);
        db.close();
    }

    public void deleteAll(){
        SQLiteDatabase db = getWritable();
        db.delete(getTableName(),null,null);
        db.close();
    }




}
