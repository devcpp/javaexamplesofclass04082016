package kkrasylnykov.com.l14_databaseandsharedprefexample.model;

import android.content.ContentValues;
import android.database.Cursor;

import kkrasylnykov.com.l14_databaseandsharedprefexample.tools_and_constants.DBConstants;

public class RecordUserInfo {

    private long m_nId = -1;
    private String m_strName = "";
    private String m_strSName = "";
    private String m_strPhone = "";

    public RecordUserInfo(String m_strName, String m_strSName, String m_strPhone) {
        this.m_nId = -1;
        this.m_strName = m_strName;
        this.m_strSName = m_strSName;
        this.m_strPhone = m_strPhone;
    }

    public RecordUserInfo(Cursor cursor){
        this.m_nId = cursor.getLong(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_ID));
        this.m_strName = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_NAME));
        this.m_strSName = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_SNAME));
        this.m_strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.TABLE_CONTACTS_FIELD_PHONE));
    }

    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public String getPhone() {
        return m_strPhone;
    }

    public void setPhone(String m_strPhone) {
        this.m_strPhone = m_strPhone;
    }

    @Override
    public String toString() {
        return "strName: " + m_strName +
                "\nstrSName: " + m_strSName
                + "\nstrPhone: " + m_strPhone+"\n";
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.TABLE_CONTACTS_FIELD_NAME, m_strName);
        values.put(DBConstants.TABLE_CONTACTS_FIELD_SNAME, m_strSName);
        values.put(DBConstants.TABLE_CONTACTS_FIELD_PHONE, m_strPhone);

        return values;
    }
}
