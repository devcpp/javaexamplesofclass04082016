package com.kkrasylnykov.l35_facebookexample;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;


public class ShowInfoActivity extends AppCompatActivity {
    private ImageView m_photo = null;
    private TextView m_name = null;
    private TextView m_email = null;

    private EditText m_title = null;
    private EditText m_text = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_info);

        m_photo = (ImageView) findViewById(R.id.userPhoto);
        m_name = (TextView) findViewById(R.id.userFullName);
        m_email = (TextView) findViewById(R.id.userEmial);

        m_title = (EditText) findViewById(R.id.Title);
        m_text = (EditText) findViewById(R.id.Text);

        findViewById(R.id.btnShare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Bitmap photoBitmap = BitmapFactory.decodeResource(getResources(),
                        R.drawable.share_image);*/

                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle(m_title.getText().toString())
                        .setContentDescription(m_text.getText().toString())
                        .setContentUrl(Uri.parse("http://spalah.ua/kh/school/"))
                        .build();

                /*SharePhoto photo = new SharePhoto.Builder()
                        .setBitmap(photoBitmap)
                        .build();
                ShareMediaContent shareContent = new ShareMediaContent.Builder()
                        .addMedium(photo)
                        .build();*/

                ShareDialog dialog  = new ShareDialog(ShowInfoActivity.this);
                dialog.show(linkContent, ShareDialog.Mode.AUTOMATIC);
            }
        });

        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        GraphRequest request = GraphRequest.newMeRequest(accessToken,new GraphRequest.GraphJSONObjectCallback(){

            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                if (response.getError() != null) {
                    Toast.makeText(ShowInfoActivity.this, "response.error -> " + response.getError().toString(), Toast.LENGTH_LONG).show();
                } else {
                    Log.d("devcppFacebook", "object -> " + object.toString());
                    String first_name = object.optString("first_name");
                    String last_name = object.optString("last_name");
                    String email = object.optString("email");
                    final String id = object.optString("id");

                    m_name.setText(first_name + " " + last_name);
                    m_email.setText(email);
                    new Thread(new Runnable() {
                        Bitmap userPhote = null;
                        @Override
                        public void run() {
                            String urldisplay = "http://graph.facebook.com/"
                                    + id + "/picture?type=large";
                            try {
                                URL url = new URL(urldisplay);
                                userPhote = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                            } catch (Exception e) {
                                Log.e("Error", e.getMessage());
                                e.printStackTrace();
                            }

                            if (userPhote!=null){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        m_photo.setImageBitmap(userPhote);
                                    }
                                });
                            }
                        }
                    }).start();

                    Log.d("devcppFacebook", "You login id -> " + id + "\nYour email: " + email);
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
        request.setParameters(parameters);
        request.executeAsync();


        AccessTokenTracker tracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken==null){
                    Intent intent = new Intent(ShowInfoActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    android.os.Process.killProcess(android.os.Process.myPid());
                }
            }
        };

        //LoginManager.getInstance().logOut();
    }
}
